package org.elsys.station;

import org.eclipse.gef.palette.CombinedTemplateCreationEntry;
import org.eclipse.gef.palette.MarqueeToolEntry;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteToolbar;
import org.eclipse.gef.palette.PanningSelectionToolEntry;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gef.requests.SimpleFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.elsys.station.model.LeftSwitch;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RightSwitch;

final class StationEditorPaletteFactory {

	private static PaletteContainer createShapesDrawer() {
		PaletteDrawer componentsDrawer = new PaletteDrawer("Elements");
		
		CombinedTemplateCreationEntry component = new CombinedTemplateCreationEntry("Rail",
				"Create a rail figure", Rail.class, new SimpleFactory(
						Rail.class), ImageDescriptor.createFromFile(
						Activator.class, "icons/rail.png"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/rail.png"));
		componentsDrawer.add(component);
		
		component = new CombinedTemplateCreationEntry(
				"Left Switch", "Create a left switch figure", LeftSwitch.class,
				new SimpleFactory(LeftSwitch.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/leftSwitch.png"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/leftSwitch.png"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry(
				"Right Switch", "Create a right switch figure", RightSwitch.class,
				new SimpleFactory(RightSwitch.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/rightSwitch.png"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/rightSwitch.png"));
		componentsDrawer.add(component);
		
		return componentsDrawer;
	}

	static PaletteRoot createPalette() {
		PaletteRoot palette = new PaletteRoot();
		palette.add(createToolsGroup(palette));
		palette.add(createShapesDrawer());
		return palette;
	}

	private static PaletteContainer createToolsGroup(PaletteRoot palette) {
		PaletteToolbar toolbar = new PaletteToolbar("Tools");

		ToolEntry tool = new PanningSelectionToolEntry();
		toolbar.add(tool);
		palette.setDefaultEntry(tool);

		toolbar.add(new MarqueeToolEntry());

		return toolbar;
	}

	private StationEditorPaletteFactory() {
	}

}