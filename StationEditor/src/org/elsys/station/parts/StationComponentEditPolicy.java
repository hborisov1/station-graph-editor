package org.elsys.station.parts;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ComponentEditPolicy;
import org.eclipse.gef.requests.GroupRequest;

import org.elsys.station.model.Element;
import org.elsys.station.model.StationGraph;
import org.elsys.station.model.commands.ElementDeleteCommand;

class StationComponentEditPolicy extends ComponentEditPolicy {

	protected Command createDeleteCommand(GroupRequest deleteRequest) {
		Object parent = getHost().getParent().getModel();
		Object child = getHost().getModel();
		if (parent instanceof StationGraph && child instanceof Element) {
			return new ElementDeleteCommand((StationGraph) parent, (Element) child);
		}
		return super.createDeleteCommand(deleteRequest);
	}
}