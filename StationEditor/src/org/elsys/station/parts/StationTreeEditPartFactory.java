package org.elsys.station.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

import org.elsys.station.model.Element;
import org.elsys.station.model.StationGraph;

public class StationTreeEditPartFactory implements EditPartFactory {

	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof Element) {
			return new ElementTreeEditPart((Element) model);
		}
		if (model instanceof StationGraph) {
			return new StationGraphTreeEditPart((StationGraph) model);
		}
		return null; 
	}

}
