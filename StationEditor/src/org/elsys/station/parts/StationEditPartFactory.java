package org.elsys.station.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.elsys.station.model.Element;
import org.elsys.station.model.StationGraph;

public class StationEditPartFactory implements EditPartFactory {

	public EditPart createEditPart(EditPart context, Object modelElement) {
		EditPart part = getPartForElement(modelElement);
		part.setModel(modelElement);
		return part;
	}

	private EditPart getPartForElement(Object modelElement) {
		if (modelElement instanceof StationGraph) {
			return new StationGraphEditPart();
		}
		if (modelElement instanceof Element) {
			return new ElementEditPart();
		}
		throw new RuntimeException("Can't create part for model element: "
				+ ((modelElement != null) ? modelElement.getClass().getName()
						: "null"));
	}

}