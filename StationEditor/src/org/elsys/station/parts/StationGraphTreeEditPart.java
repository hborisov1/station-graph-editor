package org.elsys.station.parts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.RootEditPart;
import org.eclipse.gef.editparts.AbstractTreeEditPart;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;

import org.elsys.station.model.ModelElement;
import org.elsys.station.model.StationGraph;

class StationGraphTreeEditPart extends AbstractTreeEditPart implements
		PropertyChangeListener {

	StationGraphTreeEditPart(StationGraph model) {
		super(model);
	}

	public void activate() {
		if (!isActive()) {
			super.activate();
			((ModelElement) getModel()).addPropertyChangeListener(this);
		}
	}

	protected void createEditPolicies() {
		if (getParent() instanceof RootEditPart) {
			installEditPolicy(EditPolicy.COMPONENT_ROLE,
					new RootComponentEditPolicy());
		}
	}

	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((ModelElement) getModel()).removePropertyChangeListener(this);
		}
	}

	private StationGraph getCastedModel() {
		return (StationGraph) getModel();
	}

	private EditPart getEditPartForChild(Object child) {
		return (EditPart) getViewer().getEditPartRegistry().get(child);
	}

	protected List getModelChildren() {
		return getCastedModel().getChildren(); 
	}

	public void propertyChange(PropertyChangeEvent evt) {
		String prop = evt.getPropertyName();
		if (StationGraph.CHILD_ADDED_PROP.equals(prop)) {
			addChild(createChild(evt.getNewValue()), -1);
		} else if (StationGraph.CHILD_REMOVED_PROP.equals(prop)) {
			removeChild(getEditPartForChild(evt.getNewValue()));
		} else {
			refreshVisuals();
		}
	}
}
