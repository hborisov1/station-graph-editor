package org.elsys.station.parts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.swt.graphics.Image;

import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editparts.AbstractTreeEditPart;

import org.elsys.station.model.ModelElement;
import org.elsys.station.model.Element;

class ElementTreeEditPart extends AbstractTreeEditPart implements
		PropertyChangeListener {

	ElementTreeEditPart(Element model) {
		super(model);
	}

	public void activate() {
		if (!isActive()) {
			super.activate();
			((ModelElement) getModel()).addPropertyChangeListener(this);
		}
	}

	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE,
				new StationComponentEditPolicy());
	}

	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((ModelElement) getModel()).removePropertyChangeListener(this);
		}
	}

	private Element getCastedModel() {
		return (Element) getModel();
	}

	protected Image getImage() {
		return getCastedModel().getIcon();
	}

	protected String getText() {
		return getCastedModel().toString();
	}

	public void propertyChange(PropertyChangeEvent evt) {
		refreshVisuals(); 
	}
}