package org.elsys.station.parts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Polyline;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.elsys.station.figures.LeftSwitchFigure;
import org.elsys.station.figures.RailFigure;
import org.elsys.station.figures.RightSwitchFigure;
import org.elsys.station.model.Element;
import org.elsys.station.model.LeftSwitch;
import org.elsys.station.model.ModelElement;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RightSwitch;

class ElementEditPart extends AbstractGraphicalEditPart implements
		PropertyChangeListener {


	public void activate() {
		if (!isActive()) {
			super.activate();
			((ModelElement) getModel()).addPropertyChangeListener(this);
		}
	}

	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE,
				new StationComponentEditPolicy());
	}

	protected IFigure createFigure() {
		IFigure f = createFigureForModel();
		f.setOpaque(true); 
		//f.setBackgroundColor(ColorConstants.black);
		
		return f;
	}

	private IFigure createFigureForModel() {
		if (getModel() instanceof LeftSwitch) {
			return new LeftSwitchFigure();
		} else if (getModel() instanceof Rail) {
			return new RailFigure();
		} else if (getModel() instanceof RightSwitch){
			return new RightSwitchFigure();
		} else {
			throw new IllegalArgumentException();
		}
	}

	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((ModelElement) getModel()).removePropertyChangeListener(this);
		}
	}

	private Element getCastedModel() {
		return (Element) getModel();
	}

	public void propertyChange(PropertyChangeEvent evt) {
		String prop = evt.getPropertyName();
		if (Element.SIZE_PROP.equals(prop) || Element.LOCATION_PROP.equals(prop)) {
			refreshVisuals();
		}
	}

	protected void refreshVisuals() {
		Rectangle bounds = new Rectangle(getCastedModel().getLocation(),
				getCastedModel().getSize());
		((GraphicalEditPart) getParent()).setLayoutConstraint(this,
				getFigure(), bounds);
	}
}