package org.elsys.station.parts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.Polyline;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;
import org.elsys.station.model.Element;
import org.elsys.station.model.LeftSwitch;
import org.elsys.station.model.ModelElement;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RightSwitch;
import org.elsys.station.model.StationGraph;
import org.elsys.station.model.commands.ElementCreateCommand;
import org.elsys.station.model.commands.ElementSetConstraintCommand;

class StationGraphEditPart extends AbstractGraphicalEditPart implements
		PropertyChangeListener {

	public void activate() {
		if (!isActive()) {
			super.activate();
			((ModelElement) getModel()).addPropertyChangeListener(this);
		}
	}

	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE,
				new RootComponentEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE,
				new ShapesXYLayoutEditPolicy());
	}

	protected IFigure createFigure() {
		Figure f = new FreeformLayer();
		f.setBorder(new MarginBorder(3));
		f.setLayoutManager(new FreeformLayout());

		for (int i = 0; i <= 1000; i += 30) {
			Polyline hLine = new Polyline();
			hLine.addPoint(new Point(i, 0));
			hLine.addPoint(new Point(i, 1000));
			f.add(hLine,
					new Rectangle(hLine.getStart(), hLine.getPreferredSize()));
			Polyline vLine = new Polyline();
			vLine.addPoint(new Point(0, i));
			vLine.addPoint(new Point(1000, i));
			f.add(vLine,
					new Rectangle(vLine.getStart(), vLine.getPreferredSize()));
		}
		
		return f;
	}

	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((ModelElement) getModel()).removePropertyChangeListener(this);
		}
	}

	private StationGraph getCastedModel() {
		return (StationGraph) getModel();
	}

	protected List getModelChildren() {
		return getCastedModel().getChildren(); // return a list of shapes
	}

	public void propertyChange(PropertyChangeEvent evt) {
		String prop = evt.getPropertyName();
		if (StationGraph.CHILD_ADDED_PROP.equals(prop)
				|| StationGraph.CHILD_REMOVED_PROP.equals(prop)) {
			refreshChildren();
		}
	}

	private static class ShapesXYLayoutEditPolicy extends XYLayoutEditPolicy {
		protected Command createChangeConstraintCommand(
				ChangeBoundsRequest request, EditPart child, Object constraint) {
			if (child instanceof ElementEditPart
					&& constraint instanceof Rectangle) {
				return new ElementSetConstraintCommand((Element) child.getModel(),
						request, (Rectangle) constraint);
			}
			return super.createChangeConstraintCommand(request, child,
					constraint);
		}

		protected Command createChangeConstraintCommand(EditPart child,
				Object constraint) {
			return null;
		}

		protected Command getCreateCommand(CreateRequest request) {
			Object childClass = request.getNewObjectType();
			if (childClass == LeftSwitch.class
					|| childClass == Rail.class || childClass == RightSwitch.class) {
				return new ElementCreateCommand((Element) request.getNewObject(),
						(StationGraph) getHost().getModel(),
						(Rectangle) getConstraintFor(request));
			}
			return null;
		}

	}

}