package org.elsys.station.model;

import org.eclipse.swt.graphics.Image;

public class LeftSwitch extends Element {

	private static final Image LEFT_SWITCH_ICON = createImage("icons/leftSwitch.png");

	private static final long serialVersionUID = 1;

	public Image getIcon() {
		return LEFT_SWITCH_ICON;
	}

	public String toString() {
		return "Left Switch " + hashCode();
	}
}
