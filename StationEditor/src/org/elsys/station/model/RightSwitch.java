package org.elsys.station.model;

import org.eclipse.swt.graphics.Image;

public class RightSwitch extends Element {

	private static final Image RIGHT_SWITCH_ICON = createImage("icons/rightSwitch.png");

	private static final long serialVersionUID = 1;

	public Image getIcon() {
		return RIGHT_SWITCH_ICON;
	}

	public String toString() {
		return "Right Switch " + hashCode();
	}
}