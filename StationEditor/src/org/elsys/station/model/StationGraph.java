package org.elsys.station.model;

import java.util.ArrayList;
import java.util.List;

public class StationGraph extends ModelElement {

	public static final String CHILD_ADDED_PROP = "StationGraph.ChildAdded";
	public static final String CHILD_REMOVED_PROP = "StationGraph.ChildRemoved";
	
	private static final long serialVersionUID = 1;
	
	private List elements = new ArrayList();

	public boolean addChild(Element s) {
		if (s != null && elements.add(s)) {
			firePropertyChange(CHILD_ADDED_PROP, null, s);
			return true;
		}
		return false;
	}

	public List getChildren() {
		return elements;
	}

	public boolean removeChild(Element s) {
		if (s != null && elements.remove(s)) {
			firePropertyChange(CHILD_REMOVED_PROP, null, s);
			return true;
		}
		return false;
	}
}