package org.elsys.station.model.commands;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;

import org.eclipse.gef.commands.Command;

import org.elsys.station.model.Element;
import org.elsys.station.model.StationGraph;

public class ElementCreateCommand extends Command {

	private Element newElement;
	private final StationGraph graph;
	private Rectangle bounds;

	public ElementCreateCommand(Element newElement, StationGraph graph,
			Rectangle bounds) {
		this.newElement = newElement;
		this.graph = graph;
		this.bounds = bounds;
		setLabel("element creation");
	}

	public boolean canExecute() {
		return newElement != null && graph != null && bounds != null;
	}

	public void execute() {
		newElement.setLocation(bounds.getLocation());
		Dimension size = bounds.getSize();
		if (size.width > 0 && size.height > 0)
			newElement.setSize(size);
		redo();
	}

	public void redo() {
		graph.addChild(newElement);
	}

	public void undo() {
		graph.removeChild(newElement);
	}

}