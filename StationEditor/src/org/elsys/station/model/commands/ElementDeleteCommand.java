package org.elsys.station.model.commands;

import org.eclipse.gef.commands.Command;
import org.elsys.station.model.Element;
import org.elsys.station.model.StationGraph;

public class ElementDeleteCommand extends Command {

	private final Element element;

	private final StationGraph graph;

	private boolean wasRemoved;

	public ElementDeleteCommand(StationGraph graph, Element element) {
		if (graph == null || element == null) {
			throw new IllegalArgumentException();
		}
		setLabel("element deletion");
		this.graph = graph;
		this.element = element;
	}

	public boolean canUndo() {
		return wasRemoved;
	}

	public void execute() {
		redo();
	}

	public void redo() {
		wasRemoved = graph.removeChild(element);
	}

	public void undo() {
	}
}