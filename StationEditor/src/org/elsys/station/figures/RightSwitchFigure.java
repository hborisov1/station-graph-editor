package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.PolygonShape;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

public class RightSwitchFigure extends PolygonShape {

//	private LineBorder lineBorder;
	
	public RightSwitchFigure() {

		Rectangle r = new Rectangle(0, 0, 30, 30);
//		setLayoutManager(new StackLayout());
		setStart(r.getBottom());
		addPoint(r.getBottom());
		addPoint(new Point(30, 10));
		addPoint(new Point(30, 0));
		addPoint(new Point(15, 20));
		addPoint(r.getBottom());
		setEnd(r.getBottom());
		setFill(true);
		setBackgroundColor(ColorConstants.black);
		setPreferredSize(r.getSize());

//		PolygonShape p = new PolygonShape();
//		p.setStart(new Point(24, 43));
//		p.addPoint(new Point(23, 43));
//		p.addPoint(new Point(43, 12));
//		p.addPoint(new Point(43, 4));
//		p.addPoint(new Point(24, 33));
//		p.addPoint(new Point(24, 42));
//		p.setEnd(new Point(24, 43));
//		p.setFill(true);
//		p.setBackgroundColor(ColorConstants.black);
//		p.setPreferredSize(r.getSize().expand(1, 1));
//		add(p);


		//lineBorder = new LineBorder(1);
		//lineBorder.setColor(ColorConstants.white);
		//setBorder(lineBorder);

	}

	public void setSelected(boolean selected) {
	//	lineBorder.setColor(selected ? ColorConstants.red
	//			: ColorConstants.white);
		//lineBorder.setWidth(selected ? 2 : 1);
	//	erase();
	}

}
