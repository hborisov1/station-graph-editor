package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.PolygonShape;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

public class RailFigure extends PolygonShape {
	
	private LineBorder lineBorder;
	
	public RailFigure(){
		Rectangle r = new Rectangle(0,0,30,30);
		setBackgroundColor(ColorConstants.black);
		setStart(r.getBottomRight());
		addPoint(r.getBottomRight());
		addPoint(new Point(30,20));
		addPoint(new Point(0,20));
		addPoint(r.getBottomLeft());
		setEnd(r.getBottomLeft());
		setFill(true);
		setPreferredSize(r.getSize());
		//setBorder(new LineBorder(ColorConstants.yellow, 1));
		
		//setPreferredSize(30, 10);
		//setSize(30, 10);
//		lineBorder = new LineBorder(1);
//		lineBorder.setColor(ColorConstants.white);
//		setBorder(lineBorder);
	}

	public void setSelected(boolean selected) {
//		lineBorder.setColor(selected ? ColorConstants.red : ColorConstants.white);
//		erase();
	}
	
}
