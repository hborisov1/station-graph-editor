package org.elsys.station.rcp;

import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

public class StationGraphWorkbenchAdvisor extends WorkbenchAdvisor {

	public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(
			IWorkbenchWindowConfigurer configurer) {
		return new StationGraphWorkbenchWindowAdvisor(configurer);
	}
	
	public String getInitialWindowPerspectiveId() {
		return "org.elsys.station";
	}

}
