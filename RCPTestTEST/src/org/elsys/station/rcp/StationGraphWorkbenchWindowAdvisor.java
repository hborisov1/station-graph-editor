package org.elsys.station.rcp;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.eclipse.ui.part.FileInPlaceEditorInput;

public class StationGraphWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

	public StationGraphWorkbenchWindowAdvisor(
			IWorkbenchWindowConfigurer configurer) {
		super(configurer);
	}

	public void postWindowCreate() {
		try {
			IFile file = ResourcesPlugin.getWorkspace().getRoot()
					.getFile(new Path("/project/file.cska"));
			getWindowConfigurer()
					.getWindow()
					.getActivePage()
					.openEditor(new FileInPlaceEditorInput(file),
							"org.elsys.station");
		} catch (PartInitException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void preWindowOpen() {
		getWindowConfigurer().setInitialSize(new Point(1000, 600));
		getWindowConfigurer().setShowCoolBar(false);
		getWindowConfigurer().setShowStatusLine(false);
		getWindowConfigurer().setShowMenuBar(false);
	}

}
