package org.elsys.station.model;

import org.eclipse.swt.graphics.Image;

public class RightSwitch2 extends StationElement{

	private static final long serialVersionUID = 1L;
	
	private static final Image RIGHT_SWITCH_2_ICON = createImage("icons/rightSwitch.png");

	@Override
	public Image getIcon() {
		return RIGHT_SWITCH_2_ICON;
	}
	
	@Override
	public String toString() {
		return "Right Switch 2 " + hashCode();
	}
	

}
