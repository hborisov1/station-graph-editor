package org.elsys.station.model;

import org.eclipse.swt.graphics.Image;

public class RedIndicator extends StationElement {

	private static final long serialVersionUID = 1L;

	private static final Image RED_INDICATOR_ICON = createImage("icons/rail.png");
	
	@Override
	public Image getIcon() {
		return RED_INDICATOR_ICON;
	}
	
	@Override
	public String toString() {
		return "Red Indicator " + hashCode();
	}

}
