package org.elsys.station.model;

import org.eclipse.swt.graphics.Image;

public class Rail extends StationElement {

	private static final Image RAIL_ICON = createImage("icons/rail.png");

	private static final long serialVersionUID = 1;

	public Image getIcon() {
		return RAIL_ICON;
	}

	public String toString() {
		return "Rail " + hashCode();
	}
}