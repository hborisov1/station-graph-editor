package org.elsys.station.model;

import org.eclipse.swt.graphics.Image;

public class GreenIndicator extends StationElement {

	private static final long serialVersionUID = 1L;

	private static final Image GREEN_INDICATOR_ICON = createImage("icons/rail.png");

	@Override
	public Image getIcon() {
		return GREEN_INDICATOR_ICON;
	}
	
	@Override
	public String toString() {
		return "Green Indicator " + hashCode();
	}

}
