package org.elsys.station.model;

import org.eclipse.swt.graphics.Image;


public class RailText extends StationElement {

	private static final long serialVersionUID = 1L;
	
	@Override
	public String toString() {
		return "RailText " + hashCode();
	}
	
//	public RailText(String s){
//		setText(s);
//	}
	
	public static final String TEXT_CHANGED_PROP = "RailText.TextChanged";
	
	private String text = "text";
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
		firePropertyChange(TEXT_CHANGED_PROP, null, text);
	}

	@Override
	public Image getIcon() {
		// TODO Auto-generated method stub
		return null;
	}


}
