package org.elsys.station.model;

import org.eclipse.swt.graphics.Image;

public class LeftSwitch1 extends StationElement {

	private static final long serialVersionUID = 1L;
	
	private static final Image LEFT_SWITCH_1_ICON = createImage("icons/leftSwitch.png");
	
	@Override
	public Image getIcon() {
		return LEFT_SWITCH_1_ICON;
	}
	
	@Override
	public String toString() {
		return "Left Switch 1 " + hashCode();
	}

}
