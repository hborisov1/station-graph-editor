package org.elsys.station.model;

import java.util.ArrayList;
import java.util.List;

public class Square extends ModelElement{

	public static final String SNAP_ADDED_PROP = "Square.SnapAdded";
	public static final String SNAP_REMOVED_PROP = "Square.SnapRemoved";
	
	private static final long serialVersionUID = 1L;

	private int x;
	private int y;
	
	private List snapElements = new ArrayList();
	
	public Square(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public int getX(){
		return x;
	}
	
	public int getY(){
		return y;
	}
	
	public List getSnaps(){
		return this.snapElements;
	}
	
	public boolean addSnapElement(StationElement snapElement){
		if(snapElement!=null && snapElements.add(snapElement)){
			firePropertyChange(SNAP_ADDED_PROP, null, snapElement);
			return true;
		}
		return false;
	}	
	
	public boolean removeSnapElement(StationElement snapElement){
		if(snapElement!=null && snapElements.remove(snapElement)){
			firePropertyChange(SNAP_REMOVED_PROP, null, snapElement);
			return true;
		}
		return false;
	}

	public void addSnapElement(int i, Square square) {
		// TODO Auto-generated method stub
		
	}
	
}
