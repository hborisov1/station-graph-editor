package org.elsys.station.model;

import org.eclipse.swt.graphics.Image;

public class YellowIndicator extends StationElement{

	private static final long serialVersionUID = 1L;

	private static final Image YELLOW_INDICATOR_ICON = createImage("icons/rail.png");
	
	@Override
	public Image getIcon() {
		return YELLOW_INDICATOR_ICON;
	}
	
	@Override
	public String toString() {
		return "Yellow Indicator " + hashCode();
	}

}
