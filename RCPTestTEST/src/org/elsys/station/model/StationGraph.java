package org.elsys.station.model;

import java.util.ArrayList;
import java.util.List;

public class StationGraph extends ModelElement{

	public static final String CHILD_ADDED_PROP = "StationGraph.ChildAdded";
	public static final String CHILD_REMOVED_PROP = "StationGraph.ChildRemoved";

	private static final long serialVersionUID = 1L;
	
	private List elements = new ArrayList();

	public StationGraph() {
		createElements();
	}

	protected void createElements() {
		for(int x = 0; x<=1000; x=x+39)
			for(int y=0; y<=600;y=y+39){
				Square element = new Square(x,y);
				getElements().add(element);
			}
	}

	public List getElements() {
		return this.elements;
	}
	
	public boolean addChild(StationElement elem){
		if(elem!= null && elements.add(elem)){
			firePropertyChange(CHILD_ADDED_PROP, null, elem);
			return true;
		}
		return false;
	}

	public boolean removeChild(StationElement elem){
		if(elem != null && elements.remove(elem)){
			firePropertyChange(CHILD_REMOVED_PROP, null, elem);
			return true;
		}
		return false;
	}
	
}
