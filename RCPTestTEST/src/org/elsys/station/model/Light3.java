package org.elsys.station.model;

import org.eclipse.swt.graphics.Image;

public class Light3 extends StationElement {

	private static final long serialVersionUID = 1L;
	
	private static final Image LIGHT3_ICON = createImage("icons/rail.png");

	@Override
	public Image getIcon() {
		return LIGHT3_ICON;
	}
	
	@Override
	public String toString() {
		return "Light3 " + hashCode();
	}

}
