package org.elsys.station.model;

import org.eclipse.swt.graphics.Image;

public class Light1 extends StationElement {

	private static final long serialVersionUID = 1L;

	private static final Image LIGHT1_ICON = createImage("icons/rail.png");
	
	@Override
	public Image getIcon() {
		return LIGHT1_ICON;
	}
	
	@Override
	public String toString() {
		return "Light1 " + hashCode();
	}

}
