package org.elsys.station.factory;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.elsys.station.editparts.ElementsEditPart;
import org.elsys.station.editparts.RailTextEditPart;
import org.elsys.station.editparts.SquareEditPart;
import org.elsys.station.editparts.StationGraphEditPart;
import org.elsys.station.model.LeftSwitch1;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RailText;
import org.elsys.station.model.Square;
import org.elsys.station.model.StationElement;
import org.elsys.station.model.StationGraph;

public class StationGraphEditPartFactory implements EditPartFactory {

	public EditPart createEditPart(EditPart context, Object model) {
		EditPart editPart = null;
		if (model instanceof StationGraph) {
			editPart = new StationGraphEditPart();
		} else if (model instanceof Square) {
			editPart = new SquareEditPart();
		} else if(model instanceof StationElement && !(model instanceof RailText)){
			editPart = new ElementsEditPart();
		} else if (model instanceof RailText){
			editPart = new RailTextEditPart();
		}

		if (editPart != null) {
			editPart.setModel(model);
		}

		return editPart;
	}

}
