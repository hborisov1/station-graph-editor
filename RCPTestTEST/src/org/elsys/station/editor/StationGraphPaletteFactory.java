package org.elsys.station.editor;

import org.eclipse.gef.palette.CombinedTemplateCreationEntry;
import org.eclipse.gef.palette.MarqueeToolEntry;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteToolbar;
import org.eclipse.gef.palette.PanningSelectionToolEntry;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gef.requests.SimpleFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.elsys.station.model.BlueButton;
import org.elsys.station.model.GreenButton;
import org.elsys.station.model.GreenIndicator;
import org.elsys.station.model.LeftSwitch1;
import org.elsys.station.model.LeftSwitch2;
import org.elsys.station.model.Light1;
import org.elsys.station.model.Light2;
import org.elsys.station.model.Light3;
import org.elsys.station.model.Light4;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RailText;
import org.elsys.station.model.RedButton;
import org.elsys.station.model.RedIndicator;
import org.elsys.station.model.RightSwitch1;
import org.elsys.station.model.RightSwitch2;
import org.elsys.station.model.WhiteButton;
import org.elsys.station.model.YellowButton;
import org.elsys.station.model.YellowIndicator;

final class StationGraphPaletteFactory {

	private StationGraphPaletteFactory() {
	}

	static PaletteRoot createPalette() {
		PaletteRoot palette = new PaletteRoot();
		palette.add(createToolsGroup(palette));
		palette.add(createElementsDrawer(palette));
		palette.add(createLightsDrawer());
		palette.add(createIndicatorsDrawer());
		palette.add(createButtonsDrawer());
		return palette;
	}

	private static PaletteEntry createButtonsDrawer() {
		PaletteDrawer componetsDrawer = new PaletteDrawer("Buttons",
				ImageDescriptor.createFromFile(Activator.class,
						"icons/indicators.gif"));

		CombinedTemplateCreationEntry component = new CombinedTemplateCreationEntry(
				"Green Button", "Create a green button", GreenButton.class,
				new SimpleFactory(GreenButton.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/rail.png"), ImageDescriptor.createFromFile(
						Activator.class, "icons/rail.png"));
		componetsDrawer.add(component);
		
		component = new CombinedTemplateCreationEntry(
				"White Button", "Create a white button", WhiteButton.class,
				new SimpleFactory(WhiteButton.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/rail.png"), ImageDescriptor.createFromFile(
						Activator.class, "icons/rail.png"));
		componetsDrawer.add(component);
		
		component = new CombinedTemplateCreationEntry(
				"Blue Button", "Create a blue button", BlueButton.class,
				new SimpleFactory(BlueButton.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/rail.png"), ImageDescriptor.createFromFile(
						Activator.class, "icons/rail.png"));
		componetsDrawer.add(component);
		
		component = new CombinedTemplateCreationEntry(
				"Yellow Button", "Create a yellow button", YellowButton.class,
				new SimpleFactory(YellowButton.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/rail.png"), ImageDescriptor.createFromFile(
						Activator.class, "icons/rail.png"));
		componetsDrawer.add(component);
		
		component = new CombinedTemplateCreationEntry(
				"Red Button", "Create a red button", RedButton.class,
				new SimpleFactory(RedButton.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/rail.png"), ImageDescriptor.createFromFile(
						Activator.class, "icons/rail.png"));
		componetsDrawer.add(component);

		return componetsDrawer;
	}

	private static PaletteEntry createIndicatorsDrawer() {
		PaletteDrawer componentsDrawer = new PaletteDrawer("Indicators",
				ImageDescriptor.createFromFile(Activator.class,
						"icons/indicators.gif"));

		CombinedTemplateCreationEntry component = new CombinedTemplateCreationEntry(
				"Red Indicator", "Create a red indicator", RedIndicator.class,
				new SimpleFactory(RedIndicator.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/redIndicator.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/redIndicator.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Green Indicator",
				"Create a green indicator", GreenIndicator.class,
				new SimpleFactory(GreenIndicator.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/greenIndicator.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/greenIndicator.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Yellow Indicator",
				"Create a yellow indicator", YellowIndicator.class,
				new SimpleFactory(YellowIndicator.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/yellowIndicator.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/yellowIndicator.gif"));
		componentsDrawer.add(component);

		return componentsDrawer;
	}

	private static PaletteEntry createLightsDrawer() {
		PaletteDrawer componentsDrawer = new PaletteDrawer("Lights",
				ImageDescriptor.createFromFile(Activator.class,
						"icons/lights.gif"));

		CombinedTemplateCreationEntry component = new CombinedTemplateCreationEntry(
				"Light1",
				"Create a light1 figure",
				Light1.class,
				new SimpleFactory(Light1.class),
				ImageDescriptor.createFromFile(Activator.class, "icons/l1.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/light1.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Light2",
				"Create a light2 figure", Light2.class, new SimpleFactory(
						Light2.class), ImageDescriptor.createFromFile(
						Activator.class, "icons/l2.gif"),
				ImageDescriptor.createFromFile(Activator.class, "icons/l2.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Light3",
				"Create a light3 figure", Light3.class, new SimpleFactory(
						Light3.class), ImageDescriptor.createFromFile(
						Activator.class, "icons/l3.gif"),
				ImageDescriptor.createFromFile(Activator.class, "icons/l3.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Light4",
				"Create a light4 figure", Light4.class, new SimpleFactory(
						Light4.class), ImageDescriptor.createFromFile(
						Activator.class, "icons/l4.gif"),
				ImageDescriptor.createFromFile(Activator.class, "icons/l4.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry(
				"Text",
				"Create textbox",
				RailText.class,
				new SimpleFactory(RailText.class),
				ImageDescriptor.createFromFile(Activator.class, "icons/l4.gif"),
				ImageDescriptor.createFromFile(Activator.class, "icons/l4.gif"));
		componentsDrawer.add(component);

		return componentsDrawer;
	}

	private static PaletteEntry createElementsDrawer(PaletteRoot palette) {
		PaletteDrawer componentsDrawer = new PaletteDrawer(
				"Rails and Switches", ImageDescriptor.createFromFile(
						Activator.class, "icons/railsAndSwitches.gif"));

		CombinedTemplateCreationEntry component1 = new CombinedTemplateCreationEntry(
				"Rail", "Create a rail figure", Rail.class, new SimpleFactory(
						Rail.class), ImageDescriptor.createFromFile(
						Activator.class, "icons/rail.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/rail.gif"));
		componentsDrawer.add(component1);
		palette.setDefaultEntry(component1);

		CombinedTemplateCreationEntry component = new CombinedTemplateCreationEntry(
				"LS1", "Create a left switch 1 figure", LeftSwitch1.class,
				new SimpleFactory(LeftSwitch1.class),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/ls1.gif"),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/ls1.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("LS2",
				"Create a left switch 2 figure", LeftSwitch2.class,
				new SimpleFactory(LeftSwitch2.class),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/ls2.gif"),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/ls2.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("RS1",
				"Create a right switch 1 figure", RightSwitch1.class,
				new SimpleFactory(RightSwitch1.class),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/rs1.gif"),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/rs1.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("RS2",
				"Create a right switch 2 figure", RightSwitch2.class,
				new SimpleFactory(RightSwitch2.class),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/rs2.gif"),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/rs2.gif"));
		componentsDrawer.add(component);

		return componentsDrawer;
	}

	private static PaletteContainer createToolsGroup(PaletteRoot palette) {
		PaletteToolbar toolbar = new PaletteToolbar("Tools");

		ToolEntry tool = new PanningSelectionToolEntry();
		toolbar.add(tool);
		palette.setDefaultEntry(tool);

		// toolbar.add(new PanningSelectionToolEntry());

		toolbar.add(new MarqueeToolEntry());

		return toolbar;
	}

}
