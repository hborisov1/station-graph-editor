package org.elsys.station.editor;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

public class Activator extends AbstractUIPlugin {

	/** The plug-in ID */
	public static final String PLUGIN_ID = "testeste"; //$NON-NLS-1$

	/** Single plugin instance. */
	private static Activator singleton;

	/**
	 * The constructor.
	 */
	public Activator() {
		if (singleton == null) {
			singleton = this;
		}
	}
	
	/**
	 * Returns the shared plugin instance.
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return singleton;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		singleton = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		singleton = null;
		super.stop(context);
	}

}