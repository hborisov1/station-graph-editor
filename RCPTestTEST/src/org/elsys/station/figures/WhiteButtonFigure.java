package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;

public class WhiteButtonFigure extends ButtonFigure {

	public WhiteButtonFigure() {
		button.setBackgroundColor(ColorConstants.white);
	}
	
}
