package org.elsys.station.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;

public class ButtonFigure extends Figure {

	protected CheckedToggleButton button = new CheckedToggleButton(" ");
	
	public ButtonFigure() {
		setLayoutManager(new XYLayout());
		//button.setBackgroundColor(ColorConstants.green);
		add(button);
	}
	
	@Override
	protected void paintFigure(Graphics graphics) {
		setConstraint(button, new Rectangle(11,11, 15, 15));
	}
	
}
