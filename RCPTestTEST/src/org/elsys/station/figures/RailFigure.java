package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PolygonShape;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

public class RailFigure extends PolygonShape {
	
	public RailFigure(){
		Rectangle r = new Rectangle(0,0,40,40);
		setStart(new Point(0,16));
		addPoint(new Point(0,16));
		addPoint(new Point(0,24));
		addPoint(new Point(40,24));
		addPoint(new Point(40,16));
		addPoint(new Point(0,16));
		setEnd(new Point(0,16));
		setFill(true);
		setPreferredSize(r.getSize());
		setBackgroundColor(ColorConstants.black);
	}

	public void setSelected(boolean selected) {
	}
	
	@Override
	protected void paintBorder(Graphics graphics) {
		Rectangle r = getBounds();
		graphics.setForegroundColor(ColorConstants.lightGray);
		graphics.drawLine(new Point(r.getLeft().x , r.getLeft().y - 4), new Point(r.getRight().x, r.getRight().y - 4));
		graphics.drawLine(new Point(r.getLeft().x , r.getLeft().y - 4), new Point(r.getLeft().x, r.getLeft().y+5));
		graphics.drawLine(new Point(r.getLeft().x, r.getLeft().y+5), new Point(r.getRight().x, r.getRight().y+5));
		graphics.drawLine(new Point(r.getRight().x, r.getRight().y - 4), new Point(r.getRight().x, r.getRight().y+5));
	}
	
}
