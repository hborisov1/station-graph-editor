package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;

public class BlueButtonFigure extends ButtonFigure {

	public BlueButtonFigure() {
		button.setBackgroundColor(ColorConstants.blue);
	}
	
}
