package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.PolygonShape;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

public class LeftSwitch1Figure extends PolygonShape {

	public LeftSwitch1Figure(){
		Rectangle r = new Rectangle(0,0,40,40);
		setStart(new Point(0,15));
		addPoint(new Point(0,15));
		addPoint(new Point(0,25));
		addPoint(new Point(15,40));
		addPoint(new Point(25,40));
		addPoint(new Point(0,15));
		setEnd(new Point(0,15));
		setFill(true);
		setPreferredSize(r.getSize());
		setBackgroundColor(ColorConstants.black);
	}
	
}
