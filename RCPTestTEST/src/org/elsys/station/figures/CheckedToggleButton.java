package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.ToggleButton;

public class CheckedToggleButton extends ToggleButton {

	public CheckedToggleButton(String s) {
		super(s);
	}
	
	@Override
	protected void fillCheckeredRectangle(Graphics graphics) {
		super.fillCheckeredRectangle(graphics);
		graphics.setBackgroundColor(ColorConstants.red);
		graphics.setForegroundColor(ColorConstants.gray);
	}
	
}
