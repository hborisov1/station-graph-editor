package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;

public class GreenButtonFigure extends ButtonFigure {

	public GreenButtonFigure() {
		button.setBackgroundColor(ColorConstants.green);
	}
	
}
