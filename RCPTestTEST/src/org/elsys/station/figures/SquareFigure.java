package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;

public class SquareFigure extends Figure {

	private final IFigure railsContainer;
	private final LineBorder lineBorder;

	public SquareFigure(int x, int y) {

		setBackgroundColor(ColorConstants.white);
		final StackLayout layout = new StackLayout();
		setLayoutManager(layout);

		this.setLocation(new Point(x, y));
		this.setSize(new Dimension(40, 40));

		lineBorder = new LineBorder(ColorConstants.black,1);
		setBorder(lineBorder);

//		RectangleFigure rectangleFigure = new RectangleFigure();
//		rectangleFigure.setPreferredSize(20, 20);
//		add(rectangleFigure, new Rectangle(new Point(10, 10),
//				rectangleFigure.getPreferredSize()));
		
		// Add a container for elements
		railsContainer = new Figure();
		final StackLayout notesLayout = new StackLayout();
		railsContainer.setLayoutManager(notesLayout);
		add(railsContainer);

		

	}

	public IFigure getRailsContainer() {
		return railsContainer;
	}

	public Figure getRectangleFigure() {
		return this;
	}

}
