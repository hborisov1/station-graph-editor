package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;

public class RedButtonFigure extends ButtonFigure {

	public RedButtonFigure() {
		button.setBackgroundColor(ColorConstants.red);
	}
	
}
