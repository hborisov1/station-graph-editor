package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.ToggleButton;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.geometry.Rectangle;


public class RailTextFigure extends Figure {

	private Label label;
//	private ToggleButton button;

//	public RailTextFigure() {
//		setLayoutManager(new XYLayout());
//		label = new Label("label");
//		label.setBorder(new LineBorder(1));
//		add(label, new Rectangle(10,10,20,20));
//	}

	public RailTextFigure() {
//		setLayoutManager(new GridLayout(2,false));
		setLayoutManager(new ToolbarLayout());
		label = new Label();
//		button = new ToggleButton("    ");
//		button.setBounds(new Rectangle(0,0,10,10));
//		button.setBackgroundColor(ColorConstants.yellow);
//		add(button);
		add(label);
		
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		Rectangle r = getBounds().getCopy();
		setConstraint(label, new Rectangle(0, 0, r.width, r.height));
//		setConstraint(button, new Rectangle(0,0, r.width, r.height));
	}
	
	public Label getLabel() {
		return label;
	}

}
