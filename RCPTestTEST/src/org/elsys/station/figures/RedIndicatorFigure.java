package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;

public class RedIndicatorFigure extends Figure {

	public RedIndicatorFigure() {
		setLayoutManager(new XYLayout());
		
		Ellipse ellipse = new Ellipse();
		ellipse.setBackgroundColor(ColorConstants.red);
		ellipse.setForegroundColor(ColorConstants.white);
		ellipse.setPreferredSize(11,11);
		add(ellipse, new Rectangle(this.getLocation(), ellipse.getPreferredSize()));
	}
	
}
