package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

public class LightFigure3 extends Figure {

	public LightFigure3() {
		setLayoutManager(new XYLayout());

		RectangleFigure bigRect = new RectangleFigure();
		bigRect.setBackgroundColor(ColorConstants.black);
		bigRect.setForegroundColor(ColorConstants.white);
		bigRect.setPreferredSize(15, 14);
		add(bigRect,
				new Rectangle(new Point(this.getLocation().x+18, this
						.getLocation().y), bigRect.getPreferredSize()));

		RectangleFigure rectangle = new RectangleFigure();
		rectangle.setBackgroundColor(ColorConstants.black);
		rectangle.setPreferredSize(7, 6);
		add(rectangle,
				new Rectangle(new Point(this.getLocation().x + 30, this
						.getLocation().y + 4), rectangle.getPreferredSize()));
		setOpaque(true);
	}

}
