package org.elsys.station.editparts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.elsys.station.figures.GreenIndicatorFigure;
import org.elsys.station.figures.LeftSwitch1Figure;
import org.elsys.station.figures.LeftSwitch2Figure;
import org.elsys.station.figures.LightFigure1;
import org.elsys.station.figures.LightFigure2;
import org.elsys.station.figures.LightFigure3;
import org.elsys.station.figures.LightFigure4;
import org.elsys.station.figures.RailFigure;
import org.elsys.station.figures.RedIndicatorFigure;
import org.elsys.station.figures.RightSwitch1Figure;
import org.elsys.station.figures.RightSwitch2Figure;
import org.elsys.station.model.GreenIndicator;
import org.elsys.station.model.LeftSwitch1;
import org.elsys.station.model.LeftSwitch2;
import org.elsys.station.model.Light1;
import org.elsys.station.model.Light2;
import org.elsys.station.model.Light3;
import org.elsys.station.model.Light4;
import org.elsys.station.model.ModelElement;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RedIndicator;
import org.elsys.station.model.RightSwitch1;
import org.elsys.station.model.RightSwitch2;
import org.elsys.station.model.StationElement;
import org.elsys.station.model.YellowIndicator;

public class SnapEditPart extends AbstractGraphicalEditPart implements
		PropertyChangeListener {

	@Override
	protected IFigure createFigure() {
		IFigure f = createFigureForModel();
		f.setOpaque(false);
		return f;
	}

	private IFigure createFigureForModel() {
		if (getModel() instanceof Rail) {
			return new RailFigure();
		} else if (getModel() instanceof LeftSwitch1) {
			return new LeftSwitch1Figure();
		} else if (getModel() instanceof LeftSwitch2) {
			return new LeftSwitch2Figure();
		} else if (getModel() instanceof RightSwitch1) {
			return new RightSwitch1Figure();
		} else if (getModel() instanceof RightSwitch2) {
			return new RightSwitch2Figure();
		} else if (getModel() instanceof Light1) {
			return new LightFigure1();
		} else if (getModel() instanceof Light2) {
			return new LightFigure2();
		} else if (getModel() instanceof Light3) {
			return new LightFigure3();
		} else if (getModel() instanceof Light4) {
			return new LightFigure4();
		} else if (getModel() instanceof RedIndicator) {
			return new RedIndicatorFigure();
		} else if (getModel() instanceof GreenIndicator) {
			return new GreenIndicatorFigure();
		} else if (getModel() instanceof YellowIndicator) {
			return new YellowIndicatorFigure();
		} else {
			throw new IllegalArgumentException();
		}
	}

	private StationElement getCastedModel() {
		return (StationElement) getModel();
	}

	@Override
	protected void createEditPolicies() {
		// TODO Auto-generated method stub

	}

	public void activate() {
		if (!isActive()) {
			super.activate();
			((ModelElement) getModel()).addPropertyChangeListener(this);
		}
	}

	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((ModelElement) getModel()).removePropertyChangeListener(this);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String prop = evt.getPropertyName();
		if (StationElement.SIZE_PROP.equals(prop)
				|| StationElement.LOCATION_PROP.equals(prop)) {
			refreshVisuals();
		}
	}

	protected void refreshVisuals() {
		Rectangle bounds = new Rectangle(getCastedModel().getLocation(),
				getCastedModel().getSize());
		((GraphicalEditPart) getParent()).setLayoutConstraint(this,
				getFigure(), bounds);
	}

}
