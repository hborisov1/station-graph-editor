package org.elsys.station.editparts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.jface.viewers.TextCellEditor;
import org.elsys.station.editpolicy.RailTextCellEditorLocator;
import org.elsys.station.editpolicy.RailTextDirectEditManager;
import org.elsys.station.editpolicy.RailTextDirectEditPolicy;
import org.elsys.station.figures.RailTextFigure;
import org.elsys.station.model.ModelElement;
import org.elsys.station.model.RailText;
import org.elsys.station.model.StationElement;

public class RailTextEditPart extends AbstractGraphicalEditPart implements
		PropertyChangeListener {

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String prop = evt.getPropertyName();
		if (StationElement.SIZE_PROP.equals(prop)
				|| StationElement.LOCATION_PROP.equals(prop)
				|| RailText.TEXT_CHANGED_PROP.equals(prop)) {
			refreshVisuals();
		}
	}

	protected void refreshVisuals() {
		RailTextFigure figure = (RailTextFigure) getFigure();
		RailText model = (RailText) getModel();
		SquareEditPart parent = (SquareEditPart) getParent();

		figure.getLabel().setText(model.getText());
		Rectangle layout = new Rectangle(10, 10, 20, 20);
		parent.setLayoutConstraint(this, figure, layout);
	}

	@Override
	protected IFigure createFigure() {
		return new RailTextFigure();
	}

	public void activate() {
		if (!isActive()) {
			super.activate();
			((ModelElement) getModel()).addPropertyChangeListener(this);
		}
	}

	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((ModelElement) getModel()).removePropertyChangeListener(this);
		}
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE,
				new RailTextDirectEditPolicy());
	}

	@Override
	public void performRequest(Request req) {
		//ili moje if (req.getType() == RequestConstants.REQ_DIRECT_EDIT)
		if (req.getType() == RequestConstants.REQ_OPEN) {
			performDirectEditing();
		}
	}

	private void performDirectEditing() {
		Label label = ((RailTextFigure) getFigure()).getLabel();
		RailTextDirectEditManager manager = new RailTextDirectEditManager(this,
				TextCellEditor.class, new RailTextCellEditorLocator(label),
				label);
		manager.show();
	}

}
