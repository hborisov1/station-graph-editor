package org.elsys.station.editparts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gef.editpolicies.OrderedLayoutEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.elsys.station.commands.SnapElementCreateCommand;
import org.elsys.station.commands.SnapReparentCommand;
import org.elsys.station.figures.SquareFigure;
import org.elsys.station.model.BlueButton;
import org.elsys.station.model.GreenButton;
import org.elsys.station.model.GreenIndicator;
import org.elsys.station.model.LeftSwitch1;
import org.elsys.station.model.LeftSwitch2;
import org.elsys.station.model.Light1;
import org.elsys.station.model.Light2;
import org.elsys.station.model.Light3;
import org.elsys.station.model.Light4;
import org.elsys.station.model.ModelElement;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RailText;
import org.elsys.station.model.RedButton;
import org.elsys.station.model.RedIndicator;
import org.elsys.station.model.RightSwitch1;
import org.elsys.station.model.RightSwitch2;
import org.elsys.station.model.Square;
import org.elsys.station.model.StationElement;
import org.elsys.station.model.WhiteButton;
import org.elsys.station.model.YellowButton;
import org.elsys.station.model.YellowIndicator;

public class SquareEditPart extends AbstractGraphicalEditPart implements
		PropertyChangeListener {

	// public SquareEditPart(Square square){
	// setModel(square);
	// }

	@Override
	protected IFigure createFigure() {
		int x = ((Square) getModel()).getX();
		int y = ((Square) getModel()).getY();
		return new SquareFigure(x, y);
	}

	public Square getModel() {
		return (Square) super.getModel();
	}

	private SquareFigure getSquareFigure() {
		return (SquareFigure) getFigure();
	}

	@Override
	public IFigure getContentPane() {
		return getSquareFigure().getRailsContainer();
	}

	private Square getCastedModel() {
		return (Square) getModel();
	}

	@Override
	protected List getModelChildren() {
		return getCastedModel().getSnaps();
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.LAYOUT_ROLE,
				new OrderedLayoutEditPolicy() {

					@Override
					protected Command getCreateCommand(CreateRequest request) {
						Object type = request.getNewObjectType();
						if (type == Rail.class 
								|| type == LeftSwitch1.class
								|| type == LeftSwitch2.class
								|| type == RightSwitch1.class
								|| type == RightSwitch2.class
								|| type == Light1.class
								|| type == Light2.class
								|| type == Light3.class
								|| type == Light4.class
								|| type == RedIndicator.class
								|| type == GreenIndicator.class
								|| type == YellowIndicator.class
								|| type == RailText.class
								|| type == GreenButton.class
								|| type == WhiteButton.class
								|| type == BlueButton.class
								|| type == RedButton.class
								|| type == YellowButton.class) {
							StationElement snapElement = (StationElement) request
									.getNewObject();
							return new SnapElementCreateCommand((Square) getModel(),
									snapElement, null);
						}
						return null;
					}

					@Override
					protected EditPart getInsertionReference(Request request) {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					protected Command createMoveChildCommand(EditPart child,
							EditPart after) {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					protected Command createAddCommand(EditPart child,
							EditPart after) {
						Square oldContainer = (Square) child.getParent()
								.getModel();
						if (getModel() == oldContainer)
							return null;
						StationElement snapElement = (StationElement) child
								.getModel();
						SnapReparentCommand rrc = new SnapReparentCommand(
								(Square) getModel(), snapElement);
						rrc.setOldContainer(oldContainer);
						return rrc;
					}
				});
//		installEditPolicy(EditPolicy.LAYOUT_ROLE, new XYLayoutEditPolicy() {
//
//			@Override
//			protected Command getCreateCommand(CreateRequest request) {
//				Object type = request.getNewObjectType();
//				if (type == Light.class) {
//					return new ElementCreateCommand(
//							(Draggable) request.getNewObject(), 
//							(StationGraph) getHost().getModel(), 
//							(Rectangle) getConstraintFor(request));
//				}
//				return null;
//			}
//		});
	}

	@Override
	public boolean isSelectable() {
		return false;
	}

	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
		String prop = arg0.getPropertyName();
		if (Square.SNAP_ADDED_PROP.equals(prop)
				|| Square.SNAP_REMOVED_PROP.equals(prop))
			refreshChildren();

	}

	@Override
	public void activate() {
		if (!isActive()) {
			super.activate();
			((ModelElement) getModel()).addPropertyChangeListener(this);
		}
	}

	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((ModelElement) getModel()).removePropertyChangeListener(this);
		}
	}

}
