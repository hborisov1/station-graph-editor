package org.elsys.station.editparts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;
import org.elsys.station.commands.ElementCreateCommand;
import org.elsys.station.commands.ElementSetConstraintCommand;
import org.elsys.station.model.RailText;
import org.elsys.station.model.StationElement;
import org.elsys.station.model.LeftSwitch1;
import org.elsys.station.model.LeftSwitch2;
import org.elsys.station.model.Light1;
import org.elsys.station.model.Light2;
import org.elsys.station.model.ModelElement;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RightSwitch1;
import org.elsys.station.model.RightSwitch2;
import org.elsys.station.model.StationGraph;

public class StationGraphEditPart extends AbstractGraphicalEditPart implements
		PropertyChangeListener {

	@Override
	protected IFigure createFigure() {
		FreeformLayer freeformLayer = new FreeformLayer();
		freeformLayer.setLayoutManager(new FreeformLayout());
		return freeformLayer;
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE,
				new RootComponentEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new XYLayoutEditPolicy() {

			@Override
			protected Command getCreateCommand(CreateRequest request) {
				Object childClass = request.getNewObjectType();
				if (childClass == Rail.class || childClass == LeftSwitch1.class
						|| childClass == LeftSwitch2.class
						|| childClass == RightSwitch1.class
						|| childClass == RightSwitch2.class
						|| childClass == Light1.class
						|| childClass == Light2.class
						|| childClass == RailText.class) {
					return new ElementCreateCommand(
							(StationElement) request.getNewObject(),
							(StationGraph) getHost().getModel(),
							(Rectangle) getConstraintFor(request));
				}
				return null;
			}
			
			@Override
			protected Command createChangeConstraintCommand(
					ChangeBoundsRequest request, EditPart child, Object constraint) {
				if (child instanceof StationElement
						&& constraint instanceof Rectangle) {
					return new ElementSetConstraintCommand(
							(StationElement) child.getModel(), request,
							(Rectangle) constraint);
				}
				return super.createChangeConstraintCommand(request, child,
						constraint);
			}
		});

	}

	@Override
	protected List getModelChildren() {
		List elements = ((StationGraph) getModel()).getElements();
		return elements;
	}

	public void activate() {
		if (!isActive()) {
			super.activate();
			((ModelElement) getModel()).addPropertyChangeListener(this);
		}
	}

	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((ModelElement) getModel()).removePropertyChangeListener(this);
		}
	}

	public void propertyChange(PropertyChangeEvent evt) {
		String prop = evt.getPropertyName();
		if (StationGraph.CHILD_ADDED_PROP.equals(prop)
				|| StationGraph.CHILD_REMOVED_PROP.equals(prop)) {
			refreshChildren();
		}
	}

}
