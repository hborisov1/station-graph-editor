package org.elsys.station.editparts;

import org.eclipse.draw2d.ColorConstants;
import org.elsys.station.figures.ButtonFigure;

public class YellowButtonFigure extends ButtonFigure {

	public YellowButtonFigure() {
		setBackgroundColor(ColorConstants.yellow);
	}
	
}
