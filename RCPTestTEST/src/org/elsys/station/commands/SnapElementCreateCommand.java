package org.elsys.station.commands;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.elsys.station.model.StationElement;
import org.elsys.station.model.Square;

public class SnapElementCreateCommand extends Command {

	private final Square square;
	private final StationElement snapElement;
	private final Rectangle box;
	
	public SnapElementCreateCommand(Square s, StationElement snapElement, Rectangle box) {
		super("Create snapElement");
		this.square = s;
		this.snapElement = snapElement;
		this.box = box;
//		setLabel("snapElement creation");
	}
	
	
	@Override
	public void execute() {
		if(box!=null){
			snapElement.setLocation(box.getLocation());
			snapElement.setSize(box.getSize());
		}
		square.addSnapElement(snapElement);
	}
	
	@Override
	public void undo() {
		square.removeSnapElement(snapElement);
	}
	
}
