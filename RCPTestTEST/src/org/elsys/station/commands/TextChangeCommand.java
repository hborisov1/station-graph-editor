package org.elsys.station.commands;

import org.eclipse.gef.commands.Command;
import org.elsys.station.model.RailText;

public class TextChangeCommand extends Command {

	private String oldName, newName;
	private RailText model;

	@Override
	public void execute() {
		oldName = model.getText();
		model.setText(newName);
	}

	@Override
	public void undo() {
		model.setText(oldName);
	}

	public void setNewName(String newName) {
		this.newName = newName;
	}

	public void setModel(RailText model) {
		this.model = model;
	}

}
