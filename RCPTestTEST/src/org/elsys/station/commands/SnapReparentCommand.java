package org.elsys.station.commands;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.elsys.station.model.StationElement;
import org.elsys.station.model.Square;

public class SnapReparentCommand extends Command {
	
	private final Square container;
	private final StationElement snapElement;
	private Rectangle box;
	private Square oldContainer;
	private Rectangle oldBox;

	public SnapReparentCommand(Square container, StationElement snapElement) {
		super("Move snapElement");
		this.container = container;
		this.snapElement = snapElement;
	}

	public void setOldContainer(Square container) {
		oldContainer = container;
	}
	
	public void setBox(Rectangle box) {
		this.box = box;
	}
	
	@Override
	public void execute() {
		oldBox = new Rectangle(snapElement.getLocation(), snapElement.getSize());
		oldContainer.removeSnapElement(snapElement);
		if(box!=null){
			snapElement.setLocation(box.getLocation());
			snapElement.setSize(box.getSize());
		}
		container.addSnapElement(snapElement);
	}
	
	@Override
	public void undo() {
		container.removeSnapElement(snapElement);
		oldContainer.addSnapElement(snapElement);
		snapElement.setSize(oldBox.getSize());
		snapElement.setLocation(oldBox.getLocation());
	}

}

