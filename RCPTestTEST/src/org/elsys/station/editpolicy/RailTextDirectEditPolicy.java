package org.elsys.station.editpolicy;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;
import org.elsys.station.commands.TextChangeCommand;
import org.elsys.station.figures.RailTextFigure;
import org.elsys.station.model.RailText;

public class RailTextDirectEditPolicy extends DirectEditPolicy {

	@Override
	protected Command getDirectEditCommand(DirectEditRequest request) {
		TextChangeCommand command = new TextChangeCommand();
	    command.setModel((RailText) getHost().getModel());
	    command.setNewName((String) request.getCellEditor().getValue());
	    return command;
	}

	@Override
	protected void showCurrentEditValue(DirectEditRequest request) {
		String value = (String) request.getCellEditor().getValue();
	    ((RailTextFigure)getHostFigure()).getLabel().setText(value);
	}

}
