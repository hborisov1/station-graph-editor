Running the RCP test application:
-import RCPTestTEST in eclipse
-select Run -> Run Configuration
-create a new launch configuration
-in Program to Run select Run an application and then from the dropdown menu select org.elsys.station
-select Run
-start drawing

Running the RailwayStationDiagram:
-import RailwayStationDiagram in eclipse
-select folder META-INF
-select MANIFEST.MF
-from the overview tab in the Testing section, select Launch an Eclipse application
-create new project in the new workbench
-create new file using wizard Railway Station Diagram and add it in the newly created project
-start drawing