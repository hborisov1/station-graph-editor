package org.elsys.station.model;

/**
 * An element of {@link StationGraph} that has location and size.
 * This is the abstract superclass of all other elements
 * 
 * @author Hristo Borisov
 *
 */
public abstract class StationElement {

	private int x, y, width, height;

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public boolean setSize(int newWidth, int newHeight) {
		if (width == newWidth && height == newHeight)
			return false;
		width = newWidth;
		height = newHeight;
		fireSizeChanged(width, height);
		return true;
	}

	public boolean setLocation(int newX, int newY) {
		if (x == newX && y == newY)
			return false;
		x = newX;
		y = newY;
		fireLocationChanged(x, y);
		return true;
	}

	protected abstract void fireSizeChanged(int newWidth, int newHeight);

	protected abstract void fireLocationChanged(int newX, int newY);

}
