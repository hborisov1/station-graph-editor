package org.elsys.station.model;

import java.util.Collection;
import java.util.HashSet;

import org.elsys.station.model.listener.StationElementListener;

/**
 * Rail in the {@link StationGraph}
 * 
 * @author Hristo Borisov
 *
 */
public class Rail extends StationElement {

	private final Collection<StationElementListener> listeners = new HashSet<StationElementListener>();

	public Rail(){
	}
	
	
	//============================================================
	// StationElement
	protected void fireLocationChanged(int newX, int newY) {
		for (StationElementListener l : listeners)
			l.locationChanged(newX, newY);
	}
	
	protected void fireSizeChanged(int newWidth, int newHeight) {
		for(StationElementListener l : listeners)
			l.sizeChanged(newWidth, newHeight);
	}
	
	//============================================================
	// Listeners
	public void addListener(StationElementListener l){
		listeners.add(l);
	}
	
	public void removeListener(StationElementListener l){
		listeners.remove(l);
	}

}
