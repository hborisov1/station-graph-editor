package org.elsys.station.model.listener;

/**
 * Used by {@link StationElement} to notify others when changes occur.
 * 
 * @author Hristo Borisov
 *
 */
public interface StationElementListener {
	void locationChanged(int x, int y);
	void sizeChanged(int width, int height);
}
