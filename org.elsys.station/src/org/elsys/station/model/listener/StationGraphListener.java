package org.elsys.station.model.listener;

import org.elsys.station.model.LeftSwitch;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RightSwitch;

/**
 * Used by {@link StationGraph} to notify others when changes occur.
 * 
 * @author Hristo Borisov
 *
 */
public interface StationGraphListener {

	void railAdded(Rail r);
	void railRemoved(Rail r);
	void leftSwitchAdded(LeftSwitch s);
	void leftSwitchRemoved(LeftSwitch s);
	void rightSwitchAdded(RightSwitch s);
	void rightSwitchRemoved(RightSwitch s);
	void graphCleared();
}
