package org.elsys.station.model;

import java.util.Collection;
import java.util.HashSet;

import org.elsys.station.model.listener.StationGraphListener;

/**
 * The root model object, containing all other model objects
 * 
 * @author Hristo Borisov
 *
 */
public class StationGraph {

	private final Collection<Rail> rails = new HashSet<Rail>();
	private final Collection<LeftSwitch> leftSwitches = new HashSet<LeftSwitch>();
	private final Collection<RightSwitch> rightSwitches = new HashSet<RightSwitch>();
	private final Collection<StationGraphListener> listeners = new HashSet<StationGraphListener>();
	private final Background background = new Background();
	private final Collection<Square> squares = new HashSet<Square>();
	
	/**
	 * Discard all elements so that new information can be loaded
	 */
	public void clear(){
		rails.clear();
		leftSwitches.clear();
		rightSwitches.clear();
		for(StationGraphListener l : listeners)
			l.graphCleared();
	}
	
	//============================================================
	// Rails
	
	public Collection<Rail> getRails(){
		return rails;
	}
	
	public boolean addRail(Rail r){
		if(r==null || !rails.add(r))
			return false;
		for(StationGraphListener l : listeners)
			l.railAdded(r);
		return true;
	}
	
	public boolean removeRail(Rail r){
		if(!rails.remove(r))
			return false;
		for(StationGraphListener l : listeners)
			l.railRemoved(r);
		return true;
	}
	
	//============================================================
	// Left Switches
	
	public Collection<LeftSwitch> getLeftSwitches(){
		return leftSwitches;
	}
	
	public boolean addLeftSwitch(LeftSwitch s){
		if(s==null || !leftSwitches.add(s))
			return false;
		for(StationGraphListener l : listeners)
			l.leftSwitchAdded(s);
		return true;
	}
	
	public boolean removeLeftSwitch(LeftSwitch s){
		if(!leftSwitches.remove(s))
			return false;
		for(StationGraphListener l : listeners)
			l.leftSwitchRemoved(s);
		return true;
	}
	
	//============================================================
	// Right Switches
	
	public Collection<RightSwitch> getRightSwitches(){
		return rightSwitches;
	}
	
	public boolean addRightSwitch(RightSwitch s){
		if(s==null || !rightSwitches.add(s))
			return false;
		for(StationGraphListener l : listeners)
			l.rightSwitchAdded(s);
		return true;
	}
	
	public boolean removeRightSwitch(RightSwitch s){
		if(!rightSwitches.remove(s))
			return false;
		for(StationGraphListener l : listeners)
			l.rightSwitchRemoved(s);
		return true;
	}
	
	//============================================================
	// Listeners
	
	public void addStationGraphListener(StationGraphListener l){
		listeners.add(l);
	}
	
	public void removeStationGraphListener(StationGraphListener l){
		listeners.remove(l);
	}
	
}
