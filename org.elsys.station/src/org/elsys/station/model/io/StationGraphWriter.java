package org.elsys.station.model.io;

import java.io.PrintWriter;
import java.util.Collection;
import java.util.Comparator;
import java.util.TreeSet;

import org.elsys.station.model.LeftSwitch;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RightSwitch;
import org.elsys.station.model.StationElement;
import org.elsys.station.model.StationGraph;

/**
 * Stores information from a {@link StationGraph} to an XML stream
 *
 * @author Hristo Borisov
 *
 */
public class StationGraphWriter {

	private static final String INDENT = "  ";

	private final StationGraph graph;
	private PrintWriter writer;

	public StationGraphWriter(StationGraph graph) {
		this.graph = graph;
	}

	public void write(PrintWriter writer) {
		this.writer = writer;
		writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		writer.println("<station>");
		writeRails();
		writeLeftSwitches();
		writeRightSwitches();
		writer.println("</station>");
	}

	private void writeRightSwitches() {
		Collection<RightSwitch> sorted = new TreeSet<RightSwitch>(new Comparator<RightSwitch>(){
			public int compare(RightSwitch s1, RightSwitch s2) {
				int delta = s1.getX() - s2.getX();
				if (delta == 0) 
					delta = s1.hashCode() - s2.hashCode();
				return delta;
			}
		});
		sorted.addAll(graph.getRightSwitches());
		for(RightSwitch s : sorted){
			writer.print(INDENT);
			writer.print("<rightSwitch");
			writePresentationInfo(s);
			writer.println("/>");
		}
	}

	private void writeLeftSwitches() {
		Collection<LeftSwitch> sorted = new TreeSet<LeftSwitch>(new Comparator<LeftSwitch>(){
			public int compare(LeftSwitch s1, LeftSwitch s2) {
				int delta = s1.getX() - s2.getX();
				if (delta == 0) 
					delta = s1.hashCode() - s2.hashCode();
				return delta;
			}
		});
		sorted.addAll(graph.getLeftSwitches());
		for(LeftSwitch s : sorted){
			writer.print(INDENT);
			writer.print("<leftSwitch");
			writePresentationInfo(s);
			writer.println("/>");
		}
	}

	private void writeRails(){
		Collection<Rail> sorted = new TreeSet<Rail>(new Comparator<Rail>() {
			public int compare(Rail r1, Rail r2){
				int delta = r1.getX() - r2.getX();
				if (delta == 0) 
					delta = r1.hashCode() - r2.hashCode();
				return delta;
			}
		});
		sorted.addAll(graph.getRails());
		for (Rail r : sorted){
			writer.print(INDENT);
			writer.print("<rail");
			writePresentationInfo(r);
			writer.println("/>");
		}
	}

	private void writePresentationInfo(StationElement elem) {
		writeAttribute("x", elem.getX());
		writeAttribute("y", elem.getY());
		writeAttribute("width", elem.getWidth());
		writeAttribute("height", elem.getHeight());
	}

	private void writeAttribute(String key, int value) {
		writer.print(" ");
		writer.print(key);
		writer.print("=\"");
		writer.print(value);
		writer.print("\"");
	}

}



















