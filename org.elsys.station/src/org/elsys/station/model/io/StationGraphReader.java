package org.elsys.station.model.io;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.elsys.station.model.LeftSwitch;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RightSwitch;
import org.elsys.station.model.StationElement;
import org.elsys.station.model.StationGraph;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * 
 * Loads information from an XML stream into a {@link StationGraph}
 *
 * @author Hristo Borisov
 *
 */
public class StationGraphReader  extends DefaultHandler{
	
	private final StationGraph graph;
	
	public StationGraphReader(StationGraph graph){
		this.graph = graph;
	}
	
	public void read(InputStream stream) throws ParserConfigurationException, SAXException, IOException{
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();
		parser.parse(stream, this);
	}
	
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if(qName.equals("rail")){
			Rail r = new Rail();
			readStationElementAttributes(r, attributes);
			graph.addRail(r);
		}
		else if(qName.equals("leftSwitch")){
			LeftSwitch s = new LeftSwitch();
			readStationElementAttributes(s, attributes);
			graph.addLeftSwitch(s);
		}
		else if(qName.equals("rightSwitch")){
			RightSwitch s = new RightSwitch();
			readStationElementAttributes(s, attributes);
			graph.addRightSwitch(s);
		}
	}
	
	private void readStationElementAttributes(StationElement elem, Attributes attributes) {
		elem.setLocation(readInt(attributes, "x"), readInt(attributes, "y"));
		elem.setSize(readInt(attributes,"width"), readInt(attributes,"height"));
	}

	private int readInt(Attributes attributes, String key) {
		String value = attributes.getValue(key);
		if(value == null)
			return -1;
		return Integer.parseInt(value);
	}
	
	
}
