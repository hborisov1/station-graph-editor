package org.elsys.station.commands;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.elsys.station.model.LeftSwitch;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RightSwitch;
import org.elsys.station.model.StationElement;

public class MoveAndResizeStationElementCommand extends Command {

	private final StationElement element;
	private final Rectangle box;
	private Rectangle oldBox;

	public MoveAndResizeStationElementCommand(StationElement element, Rectangle box) {
		this.element = element;
		this.box = box;
		setLabel("Modify " + getElementName());
	}

	private String getElementName() {
		if (element instanceof Rail)
			return "Rail";
		if (element instanceof LeftSwitch)
			return "LeftSwitch";
		if (element instanceof RightSwitch)
			return "RightSwitch";
		return "Element";
	}


	public void execute() {
		oldBox = new Rectangle(element.getX(), element.getY(),
				element.getWidth(), element.getHeight());
		element.setLocation(box.x, box.y);
		//element.setSize(box.width, box.height);
	}

	public void undo() {
		element.setLocation(oldBox.x, oldBox.y);
		element.setSize(oldBox.width, oldBox.height);
	}

}
