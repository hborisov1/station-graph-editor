package org.elsys.station.commands;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.elsys.station.model.RightSwitch;
import org.elsys.station.model.StationGraph;

public class CreateRightSwitchCommand extends Command{

	private final StationGraph graph;
	private final RightSwitch s;
	private final Rectangle box;
	
	public CreateRightSwitchCommand(StationGraph graph, RightSwitch s, Rectangle box) {
		super("Create Right Switch");
		this.graph = graph;
		this.s = s;
		this.box = box;
	}
	
	public void execute() {
		s.setLocation(box.x, box.y);
		s.setSize(box.width, box.height);
		graph.addRightSwitch(s);
	}
	
	public void undo() {
		graph.removeRightSwitch(s);
	}
	
}