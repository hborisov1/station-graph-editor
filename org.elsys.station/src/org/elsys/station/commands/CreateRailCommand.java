package org.elsys.station.commands;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.elsys.station.model.Rail;
import org.elsys.station.model.StationGraph;

public class CreateRailCommand extends Command{

	private final StationGraph graph;
	private final Rail rail;
	private final Rectangle box;
	
	public CreateRailCommand(StationGraph graph, Rail rail, Rectangle box) {
		super("Create Rail");
		this.graph = graph;
		this.rail = rail;
		this.box = box;
	}
	
	public void execute() {
		rail.setLocation(box.x, box.y);
		rail.setSize(box.width, box.height);
		graph.addRail(rail);
	}
	
	public void undo() {
		graph.removeRail(rail);
	}
	
}
