package org.elsys.station.commands;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.elsys.station.model.LeftSwitch;
import org.elsys.station.model.StationGraph;

public class CreateLeftSwitchCommand extends Command{

	private final StationGraph graph;
	private final LeftSwitch s;
	private final Rectangle box;
	
	public CreateLeftSwitchCommand(StationGraph graph, LeftSwitch s, Rectangle box) {
		super("Create Left Switch");
		this.graph = graph;
		this.s = s;
		this.box = box;
	}
	
	public void execute() {
		s.setLocation(box.x, box.y);
		s.setSize(box.width, box.height);
		graph.addLeftSwitch(s);
	}
	
	public void undo() {
		graph.removeLeftSwitch(s);
	}
	
}