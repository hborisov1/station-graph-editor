package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.PolygonShape;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

/**
 * Class for drawing right left figure
 * 
 * @author Hristo Borisov
 *
 */
public class LeftSwitchFigure extends PolygonShape {

//	private LineBorder lineBorder;
	
	public static final Image LEFT_SWITCH_IMAGE = new Image(Display.getCurrent(),
			LeftSwitchFigure.class.getResourceAsStream("leftSwitch.png"));

	public LeftSwitchFigure() {

		Rectangle r = new Rectangle(0, 0, 30, 30);
//		setLayoutManager(new StackLayout());
		setStart(r.getBottom());
		addPoint(r.getBottom());
		addPoint(new Point(0, 10));
		addPoint(new Point(0, 0));
		addPoint(new Point(15, 20));
		addPoint(r.getBottom());
		setEnd(r.getBottom());
		setFill(true);
		setBackgroundColor(ColorConstants.black);
		setPreferredSize(r.getSize());
		
//		Rectangle r = new Rectangle(0, 0, 45, 45);
//		setLayoutManager(new StackLayout());
//		setStart(r.getBottom());
//		addPoint(r.getBottom());
//		addPoint(new Point(45, 10));
//		addPoint(new Point(45, 0));
//		addPoint(new Point(22, 35));
//		addPoint(r.getBottom());
//		setEnd(r.getBottom());
//		setFill(true);
//		setBackgroundColor(ColorConstants.white);
//		setPreferredSize(r.getSize().expand(1, 1));
//
//		PolygonShape p = new PolygonShape();
//		p.setStart(new Point(24, 43));
//		p.addPoint(new Point(23, 43));
//		p.addPoint(new Point(43, 12));
//		p.addPoint(new Point(43, 4));
//		p.addPoint(new Point(24, 33));
//		p.addPoint(new Point(24, 42));
//		p.setEnd(new Point(24, 43));
//		p.setFill(true);
//		p.setBackgroundColor(ColorConstants.black);
//		p.setPreferredSize(r.getSize().expand(1, 1));
//		add(p);
		
//		lineBorder = new LineBorder(1);
//		lineBorder.setColor(ColorConstants.white);
//		setBorder(lineBorder);

	}

	public void setSelected(boolean selected) {
//		lineBorder.setColor(selected ? ColorConstants.red
//				: ColorConstants.white);
//		//lineBorder.setWidth(selected ? 2 : 1);
//		erase();
	}

}
