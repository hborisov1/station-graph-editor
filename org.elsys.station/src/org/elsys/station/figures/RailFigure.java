package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

/**
 * Class for drawing a rail figure
 * 
 * @author Hristo Borisov
 *
 */
public class RailFigure extends RectangleFigure {
	
	public static final Image RAIL_IMAGE = new Image(Display.getCurrent(),
		RailFigure.class.getResourceAsStream("rail.png"));
	
	private LineBorder lineBorder;
	
	public RailFigure(){
		setBackgroundColor(ColorConstants.red);
		setPreferredSize(32, 10);
		lineBorder = new LineBorder(1);
		lineBorder.setColor(ColorConstants.white);
		setBorder(lineBorder);
	}

	public void setSelected(boolean selected) {
		lineBorder.setColor(selected ? ColorConstants.red : ColorConstants.white);
		erase();
	}
	
}
