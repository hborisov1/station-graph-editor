package org.elsys.station.parts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.Polyline;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;
import org.elsys.station.commands.CreateLeftSwitchCommand;
import org.elsys.station.commands.CreateRailCommand;
import org.elsys.station.commands.CreateRightSwitchCommand;
import org.elsys.station.commands.MoveAndResizeStationElementCommand;
import org.elsys.station.model.LeftSwitch;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RightSwitch;
import org.elsys.station.model.StationElement;
import org.elsys.station.model.StationGraph;
import org.elsys.station.model.listener.StationGraphListener;

public class StationGraphEditPart extends AbstractGraphicalEditPart implements
		StationGraphListener {

	public StationGraphEditPart(StationGraph stationGraph) {
		setModel(stationGraph);
		stationGraph.addStationGraphListener(this);
	}

	public StationGraph getModel() {
		return (StationGraph) super.getModel();
	}

	protected IFigure createFigure() {
		Figure figure = new FreeformLayer();
		figure.setBorder(new MarginBorder(3));
		figure.setLayoutManager(new FreeformLayout());

//		// background
//		for (int i = 0; i <= 1000; i += 30) {
//			Polyline hLine = new Polyline();
//			hLine.addPoint(new Point(i, 0));
//			hLine.addPoint(new Point(i, 1000));
//			figure.add(hLine,
//					new Rectangle(hLine.getStart(), hLine.getPreferredSize()));
//			Polyline vLine = new Polyline();
//			vLine.addPoint(new Point(0, i));
//			vLine.addPoint(new Point(1000, i));
//			figure.add(vLine,
//					new Rectangle(vLine.getStart(), vLine.getPreferredSize()));
//		}

		return figure;
	}

	protected List<StationElement> getModelChildren() {
		List<StationElement> allObjects = new ArrayList<StationElement>();
		allObjects.addAll(getModel().getRails());
		allObjects.addAll(getModel().getLeftSwitches());
		allObjects.addAll(getModel().getRightSwitches());
		return allObjects;
	}

	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new XYLayoutEditPolicy() {
			protected Command getCreateCommand(CreateRequest request) {
				Object type = request.getNewObjectType();
				Rectangle box = (Rectangle) getConstraintFor(request);
				StationGraph graph = getModel();
				if (type == Rail.class) {
					Rail rail = (Rail) request.getNewObject();
					return new CreateRailCommand(graph, rail, box);
				}
				if (type == RightSwitch.class) {
					RightSwitch s = (RightSwitch) request.getNewObject();
					return new CreateRightSwitchCommand(graph, s, box);
				}
				if (type == LeftSwitch.class) {
					LeftSwitch s = (LeftSwitch) request.getNewObject();
					return new CreateLeftSwitchCommand(graph, s, box);
				}
				return null;
			}
			protected Command createChangeConstraintCommand(ChangeBoundsRequest request, EditPart child, Object constraint) {
				StationElement elem = (StationElement) child.getModel();
				Rectangle box = (Rectangle) constraint;
				return new MoveAndResizeStationElementCommand(elem, box);
			}
		});
	}

	public void railAdded(Rail r) {
		addChild(createChild(r), 0);
	}

	public void railRemoved(Rail r) {
		stationElementRemoved(r);
	}

	public void leftSwitchAdded(LeftSwitch s) {
		addChild(createChild(s), 0);
	}

	public void leftSwitchRemoved(LeftSwitch s) {
		stationElementRemoved(s);
	}

	public void rightSwitchAdded(RightSwitch s) {
		addChild(createChild(s), 0);
	}

	public void rightSwitchRemoved(RightSwitch s) {
		stationElementRemoved(s);
	}

	private void stationElementRemoved(StationElement elem) {
		Object part = getViewer().getEditPartRegistry().get(elem);
		if (part instanceof EditPart)
			removeChild((EditPart) part);
	}

	public void graphCleared() {

	}

}
