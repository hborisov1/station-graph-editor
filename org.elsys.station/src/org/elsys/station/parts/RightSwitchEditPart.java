package org.elsys.station.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.elsys.station.figures.RightSwitchFigure;
import org.elsys.station.model.RightSwitch;
import org.elsys.station.model.listener.StationElementListener;

public class RightSwitchEditPart extends StationElementEditPart implements StationElementListener {

	public RightSwitchEditPart(RightSwitch rightSwitch) {
		setModel(rightSwitch);
	}

	public RightSwitch getModel() {
		return (RightSwitch) super.getModel();
	}

	protected IFigure createFigure() {
		return new RightSwitchFigure();
	}

	protected void createEditPolicies() {

	}

	protected void fireSelectionChanged() {
		((RightSwitchFigure) getFigure()).setSelected(getSelected() != 0);
		super.fireSelectionChanged();
	}
	
	public void addNotify() {
		super.addNotify();
		getModel().addListener(this);
	}

	public void removeNotify() {
		getModel().removeListener(this);
		super.removeNotify();
	}
	
	public void locationChanged(int x, int y) {
		getFigure().setLocation(new Point(x, y));
	}

	public void sizeChanged(int width, int height) {
		getFigure().setSize(width, height);
	}

}
