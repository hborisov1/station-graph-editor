package org.elsys.station.parts;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.elsys.station.model.StationElement;

public abstract class StationElementEditPart extends AbstractGraphicalEditPart {

	protected void refreshVisuals() {
		StationElement elem = (StationElement) getModel();
		Rectangle bounds = new Rectangle(elem.getX(), elem.getY(),
				elem.getWidth(), elem.getHeight());
		((GraphicalEditPart) getParent()).setLayoutConstraint(this,
				getFigure(), bounds);
		super.refreshVisuals();
	}

}
