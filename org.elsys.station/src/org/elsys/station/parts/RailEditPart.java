package org.elsys.station.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.elsys.station.figures.RailFigure;
import org.elsys.station.model.Rail;
import org.elsys.station.model.listener.StationElementListener;

public class RailEditPart extends StationElementEditPart implements StationElementListener{

	public RailEditPart(Rail rail) {
		setModel(rail);
	}

	public Rail getModel() {
		return (Rail) super.getModel();
	}

	protected IFigure createFigure() {
		return new RailFigure();
	}

	protected void createEditPolicies() {
	}

	protected void fireSelectionChanged() {
		((RailFigure) getFigure()).setSelected(getSelected() != 0);
		super.fireSelectionChanged();
	}

	public void addNotify() {
		super.addNotify();
		getModel().addListener(this);
	}

	public void removeNotify() {
		getModel().removeListener(this);
		super.removeNotify();
	}
	
	public void locationChanged(int x, int y) {
		getFigure().setLocation(new Point(x, y));
	}

	public void sizeChanged(int width, int height) {
		getFigure().setSize(width, height);
	}

}
