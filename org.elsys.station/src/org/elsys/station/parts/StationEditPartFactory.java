package org.elsys.station.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.elsys.station.model.LeftSwitch;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RightSwitch;
import org.elsys.station.model.StationGraph;

/**
 * A factory for constructing {@link EditPart}s for station model objects
 * 
 * @author Hristo Borisov
 *
 */
public class StationEditPartFactory implements EditPartFactory {

	public EditPart createEditPart(EditPart context, Object model) {
		if(model instanceof StationGraph)
			return new StationGraphEditPart((StationGraph) model);
		if(model instanceof Rail)
			return new RailEditPart((Rail) model);
		if(model instanceof LeftSwitch)
			return new LeftSwitchEditPart((LeftSwitch) model);
		if(model instanceof RightSwitch) 
			return new RightSwitchEditPart((RightSwitch) model);
		throw new IllegalStateException("No EditPart for " + model.getClass());
	}

}
