package org.elsys.station.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.elsys.station.figures.LeftSwitchFigure;
import org.elsys.station.model.LeftSwitch;
import org.elsys.station.model.listener.StationElementListener;

public class LeftSwitchEditPart extends StationElementEditPart implements StationElementListener{

	public LeftSwitchEditPart(LeftSwitch leftSwitch){
		setModel(leftSwitch);
	}
	
	public LeftSwitch getModel(){
		return (LeftSwitch) super.getModel();
	}
	
	protected IFigure createFigure() {
		return new LeftSwitchFigure();
	}

	protected void createEditPolicies() {
	}

	protected void fireSelectionChanged() {
		((LeftSwitchFigure) getFigure()).setSelected(getSelected() != 0);
		super.fireSelectionChanged();
	}
	
	public void addNotify() {
		super.addNotify();
		getModel().addListener(this);
	}

	public void removeNotify() {
		getModel().removeListener(this);
		super.removeNotify();
	}
	
	public void locationChanged(int x, int y) {
		getFigure().setLocation(new Point(x, y));
	}

	public void sizeChanged(int width, int height) {
		getFigure().setSize(width, height);
	}
	
}
