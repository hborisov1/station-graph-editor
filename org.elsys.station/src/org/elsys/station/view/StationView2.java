package org.elsys.station.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.editparts.ScalableRootEditPart;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.elsys.station.model.StationGraph;
import org.elsys.station.model.io.StationGraphReader;
import org.elsys.station.model.io.StationGraphWriter;
import org.elsys.station.parts.StationEditPartFactory;

public class StationView2 extends GraphicalEditorWithFlyoutPalette {

	private ScrollingGraphicalViewer viewer;
	private StationGraph graph = new StationGraph();
	//private final StationGraph stationGraph = new StationGraph();
	
	//alt shift j  - nice
	
	public StationView2(){
		setEditDomain(new DefaultEditDomain(this));
	}
	
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();
		GraphicalViewer viewer = getGraphicalViewer();
		viewer.setEditPartFactory(new StationEditPartFactory());
		viewer.setRootEditPart(new ScalableFreeformRootEditPart());
	}
	
	protected void initializeGraphicalViewer() {
		super.initializeGraphicalViewer();
		getGraphicalViewer().setContents(graph);
	}
	
	public static void main(String[] args) {
		new StationView2().run();
	}

	private void run() {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setSize(600, 500);
		shell.setText("Station Editor");
		shell.setLayout(new GridLayout());

		FigureCanvas canvas = createDiagram(shell);
		canvas.setLayoutData(new GridData(GridData.FILL_BOTH));
		createMenuBar(shell);

		readAndClose(getClass().getResourceAsStream("station.xml"));

		shell.open();
		while (!shell.isDisposed()) {
			while (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	public void createPartControl(Composite parent) {
		createDiagram(parent);
		readAndClose(getClass().getResourceAsStream("station.xml"));
	}

	private FigureCanvas createDiagram(Composite parent) {
		viewer = new ScrollingGraphicalViewer();
		viewer.createControl(parent);
		viewer.setRootEditPart(new ScalableRootEditPart());
		viewer.getControl().setBackground(ColorConstants.white);
		viewer.setEditPartFactory(new StationEditPartFactory());
		return (FigureCanvas) viewer.getControl();
	}

	public void setFocus() {
	}

	private void createMenuBar(Shell shell) {

		final Menu menuBar = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menuBar);
		MenuItem fileMenuItem = new MenuItem(menuBar, SWT.CASCADE);
		fileMenuItem.setText("File");
		Menu fileMenu = new Menu(shell, SWT.DROP_DOWN);
		fileMenuItem.setMenu(fileMenu);

		createOpenFileMenuItem(fileMenu);
		createSaveFileMenuItem(fileMenu);

	}

	private void createSaveFileMenuItem(Menu menu) {
		MenuItem menuItem = new MenuItem(menu, SWT.NULL);
		menuItem.setText("Save...");
		menuItem.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}

			public void widgetSelected(SelectionEvent e) {
				saveFile();
			}
		});
	}

	private void saveFile() {
		Shell shell = Display.getDefault().getActiveShell();
		FileDialog dialog = new FileDialog(shell, SWT.SAVE);
		dialog.setText("Save");
		String path = dialog.open();
		if (path == null)
			return;
		File file = new File(path);
		if (file.exists()) {
			if (!MessageDialog.openQuestion(shell, "Overwrite?",
					"Overwrite the existing file?\n" + path))
				return;
		}
		PrintWriter writer;
		try {
			writer = new PrintWriter(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		try {
			new StationGraphWriter(graph).write(writer);
		} finally {
			writer.close();
		}
	}

	private void createOpenFileMenuItem(Menu menu) {
		MenuItem menuItem = new MenuItem(menu, SWT.NULL);
		menuItem.setText("Open...");
		menuItem.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				openFile();
			}
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});
	}

	private void openFile() {
		Shell shell = Display.getDefault().getActiveShell();
		FileDialog dialog = new FileDialog(shell, SWT.OPEN);
		dialog.setText("Select a Station Graph File");
		String path = dialog.open();
		if (path == null)
			return;
		try {
			readAndClose(new FileInputStream(path));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void readAndClose(InputStream stream) {
		StationGraph newGraph = new StationGraph();
		try {
			new StationGraphReader(newGraph).read(stream);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		setModel(newGraph);
	}

	private void setModel(StationGraph newGraph) {
		graph = newGraph;
		viewer.setContents(graph);
	}

	protected PaletteRoot getPaletteRoot() {
		return null;
	}

	public void doSave(IProgressMonitor arg0) {
	}

}
