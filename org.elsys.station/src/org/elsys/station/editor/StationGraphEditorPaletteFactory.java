package org.elsys.station.editor;

import org.eclipse.gef.palette.CombinedTemplateCreationEntry;
import org.eclipse.gef.palette.MarqueeToolEntry;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteToolbar;
import org.eclipse.gef.palette.PanningSelectionToolEntry;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gef.requests.SimpleFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.elsys.station.figures.LeftSwitchFigure;
import org.elsys.station.figures.RailFigure;
import org.elsys.station.figures.RightSwitchFigure;
import org.elsys.station.model.LeftSwitch;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RightSwitch;

/**
 * Factory for creating editor's palette
 * 
 * @author Hristo Borisov
 *
 */
public class StationGraphEditorPaletteFactory {

	private static final ImageDescriptor RAIL_IMAGE_DESCRIPTOR = ImageDescriptor
			.createFromImage(RailFigure.RAIL_IMAGE);
	private static final ImageDescriptor LEFT_SWITCH_IMAGE_DESCRIPTOR = ImageDescriptor
			.createFromImage(LeftSwitchFigure.LEFT_SWITCH_IMAGE);
	private static final ImageDescriptor RIGHT_SWITCH_IMAGE_DESCRIPTOR = ImageDescriptor
			.createFromImage(RightSwitchFigure.RIGHT_SWITCH_IMAGE);

	/**
	 * Create a new palette for the editor
	 * 
	 * @return a new PaletteRoot
	 */
	public static PaletteRoot createPalette() {
		PaletteRoot palette = new PaletteRoot();
		palette.add(createToolsGroup(palette));
		palette.add(createElementsDrawer());
		return palette;
	}
	
	/**
	 * Create a toolbar containing the common tools that appear at the top of the palette
	 * and set the selection tool as the default tool.
	 */
	private static PaletteEntry createToolsGroup(PaletteRoot palette) {
		PaletteToolbar toolbar = new PaletteToolbar("Tools");
		
		// Add a selection tool to the group
		ToolEntry tool = new PanningSelectionToolEntry();
		toolbar.add(tool);
		palette.setDefaultEntry(tool);
		
		// Add a marquee tool to the group
		toolbar.add(new MarqueeToolEntry());
		
		return toolbar;
	}

	private static PaletteEntry createElementsDrawer() {
		PaletteDrawer componentsDrawer = new PaletteDrawer("Elements");

		//Tool for creating rails
		SimpleFactory factory = new SimpleFactory(Rail.class) {
			public Object getNewObject() {
				Rail rail = new Rail();
				return rail;
			}
		};
		CombinedTemplateCreationEntry component = new CombinedTemplateCreationEntry("Rail", "Add a rail to the Station Graph", factory,	RAIL_IMAGE_DESCRIPTOR, RAIL_IMAGE_DESCRIPTOR);
		componentsDrawer.add(component);

		//Tool for creating left switches
		factory = new SimpleFactory(LeftSwitch.class){
			public Object getNewObject(){
				LeftSwitch s = new LeftSwitch();
				return s;
			}
		};
		component = new CombinedTemplateCreationEntry("Left Switch", "Add a left switch to the Station Graph", factory, LEFT_SWITCH_IMAGE_DESCRIPTOR, LEFT_SWITCH_IMAGE_DESCRIPTOR);
		componentsDrawer.add(component);
		
		//Tool for creating right switches
		factory = new SimpleFactory(RightSwitch.class){
			public Object getNewObject(){
				RightSwitch s = new RightSwitch();
				return s;
			}
		};
		component = new CombinedTemplateCreationEntry("Right Switch", "Add a right switch to the Station Graph", factory, RIGHT_SWITCH_IMAGE_DESCRIPTOR, RIGHT_SWITCH_IMAGE_DESCRIPTOR);
		componentsDrawer.add(component);
		
		
		return componentsDrawer;
	}
}
