package org.elsys.station.editor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette;
import org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.dialogs.SaveAsDialog;
import org.eclipse.ui.part.FileEditorInput;
import org.elsys.station.model.StationGraph;
import org.elsys.station.model.io.StationGraphReader;
import org.elsys.station.model.io.StationGraphWriter;
import org.elsys.station.parts.StationEditPartFactory;

/**
 * An editor for loading, modifying and saving railway station schemes
 * 
 * @author Hristo Borisov
 * 
 */
public class StationGraphEditor extends GraphicalEditorWithFlyoutPalette {

	private final StationGraph stationGraph = new StationGraph();

	/**
	 * Set the EditDomain, responsible for directing user input and managing
	 * command stack and tool palette
	 */
	public StationGraphEditor() {
		setEditDomain(new DefaultEditDomain(this));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette#getPaletteRoot
	 * ()
	 */
	protected PaletteRoot getPaletteRoot() {
		return StationGraphEditorPaletteFactory.createPalette();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.parts.GraphicalEditor#configureGraphicalViewer()
	 */
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();
		GraphicalViewer viewer = getGraphicalViewer();
		viewer.setEditPartFactory(new StationEditPartFactory());
		viewer.setRootEditPart(new ScalableFreeformRootEditPart());

		viewer.setKeyHandler(new GraphicalViewerKeyHandler(viewer));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette#
	 * initializeGraphicalViewer()
	 */
	protected void initializeGraphicalViewer() {
		super.initializeGraphicalViewer();
		getGraphicalViewer().setContents(stationGraph);
	}

	/**
	 * Calls readAndClose() to read the file content into the model
	 */
	protected void setInput(IEditorInput input) {
		super.setInput(input);
		IFile file = ((IFileEditorInput) input).getFile();
		setPartName(file.getName());
		
		//load some default content, if the file is empty
		
		try {
			InputStream stream = file.getContents();
			if (stream.read() == -1) {
				stream.close();
				readAndClose(getClass().getResourceAsStream(
						"../view/station.xml"));
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		
		// Read the content from the stream into the model
		
		try {
			readAndClose(file.getContents());
		} catch (CoreException e) {
			e.printStackTrace();
			return;
		}
	}

	/**
	 * Read the content from the stream into the model
	 * 
	 * @param stream the stream to be read and closed
	 */
	private void readAndClose(InputStream stream) {
		stationGraph.clear();
		try {
			new StationGraphReader(stationGraph).read(stream);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Saving the model
	 * 
	 * @param monitor the progress monitor
	 */
	public void doSave(IProgressMonitor monitor) {
		
		// Serialize the model
		
		StringWriter writer = new StringWriter(5000);
		new StationGraphWriter(stationGraph).write(new PrintWriter(writer));
		ByteArrayInputStream stream = new ByteArrayInputStream(writer
				.toString().getBytes());
		
		// Store the serialized model in the file
		
		IFile file = ((IFileEditorInput) getEditorInput()).getFile();
		try {
			if (file.exists())
				file.setContents(stream, false, true, monitor);
			else
				file.create(stream, false, monitor);
		} catch (CoreException e) {
			handleException(e);
			return;
		}
		
		// Update the editor state to indicate that the contents 
		// have been saved and notify all listeners about the change in state
		
		getCommandStack().markSaveLocation();
		firePropertyChange(PROP_DIRTY);
	}

	/**
	 * Notify the user that a problem has occurred
	 * 
	 * @param ex the exception that occurred
	 */
	private void handleException(Exception ex) {
		ex.printStackTrace();
		Status status = new Status(IStatus.ERROR, "org.elsys.station",
				"An exception occurred while saving the file", ex);
		ErrorDialog.openError(getSite().getShell(), "Exception",
				ex.getMessage(), status);
	}

	
	
	/**
	 * Allows doSaveAs()
	 */
	public boolean isSaveAsAllowed() {
		return true;
	}

	
	
	/**
	 * Execute when user selects File->Save As...
	 */
	public void doSaveAs() {
		
		//Prompt the user for a new file
		
		SaveAsDialog dialog = new SaveAsDialog(getSite().getShell());
		dialog.setOriginalFile(((IFileEditorInput) getEditorInput()).getFile());
		dialog.open();
		
		//If the user clicked cancel, close the dialog
		
		IPath path = dialog.getResult();
		if (path == null)
			return;
		
		//Call doSave() to save the content in the new file
		
		IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
		super.setInput(new FileEditorInput(file));
		doSave(null);
		setPartName(file.getName());
		firePropertyChange(PROP_INPUT);
	}

}
