package org.elsys.station.factory;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.elsys.station.editparts.RailwayStationElementsEditPart;
import org.elsys.station.editparts.TextFieldEditPart;
import org.elsys.station.editparts.SquareEditPart;
import org.elsys.station.editparts.SquareIndexEditPart;
import org.elsys.station.editparts.RailwayStationDiagramEditPart;
import org.elsys.station.model.TextField;
import org.elsys.station.model.Square;
import org.elsys.station.model.SquareIndex;
import org.elsys.station.model.RailwayStationElement;
import org.elsys.station.model.RailwayStationDiagram;

/**
 * Factory for creating edit parts for model elements
 * 
 * @author Hristo Borisov
 *
 */
public class RailwayStationDiagramEditPartFactory implements EditPartFactory {

	/* (non-Javadoc)
	 * @see org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart, java.lang.Object)
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		EditPart editPart = null;
		if (model instanceof RailwayStationDiagram) {
			editPart = new RailwayStationDiagramEditPart();
		} else if (model instanceof Square) {
			editPart = new SquareEditPart();
		} else if(model instanceof RailwayStationElement && !(model instanceof TextField)){
			editPart = new RailwayStationElementsEditPart();
		} else if (model instanceof TextField){
			editPart = new TextFieldEditPart();
		} else if (model instanceof SquareIndex){
			editPart = new SquareIndexEditPart();
		}
		if (editPart != null) {
			editPart.setModel(model);
		}

		return editPart;
	}

}
