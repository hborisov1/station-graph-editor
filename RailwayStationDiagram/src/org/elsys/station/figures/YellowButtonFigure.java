package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;

/**
 * Figure for displaying an yellow button
 * 
 * @author Hristo Borisov
 *
 */
public class YellowButtonFigure extends ButtonFigure {

	public YellowButtonFigure() {
		setBackgroundColor(ColorConstants.yellow);
	}
	
}
