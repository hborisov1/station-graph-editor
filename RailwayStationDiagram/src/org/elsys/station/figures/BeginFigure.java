package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

public class BeginFigure extends Figure {

	private RectangleFigure vRect;
	private RectangleFigure hRect;
	
	public BeginFigure() {
		setLayoutManager(new XYLayout());
		
		hRect = new RectangleFigure();
		hRect.setBackgroundColor(ColorConstants.lightGray);
		hRect.setPreferredSize(20, 8);
		hRect.setBorder(new LineBorder(ColorConstants.lightGray));
		add(hRect, new Rectangle(new Point(20,16), hRect.getPreferredSize()));
		
		vRect = new RectangleFigure();
		vRect.setBackgroundColor(ColorConstants.lightGray);
		vRect.setPreferredSize(2, 25);
		vRect.setBorder(new LineBorder(ColorConstants.lightGray));
		add(vRect, new Rectangle(new Point(19,7), vRect.getPreferredSize()));
	}
	
}
