package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * Figure for displaying a right exit light
 * 
 * @author Hristo Borisov
 *
 */
public class RightExitLightFigure extends Figure {

	public RightExitLightFigure() {
		setLayoutManager(new XYLayout());
		
		Ellipse ellipse = new Ellipse();
		ellipse.setBackgroundColor(ColorConstants.black);
		ellipse.setForegroundColor(ColorConstants.white);
		ellipse.setPreferredSize(15, 14);
		add(ellipse,
				new Rectangle(this.getLocation(), ellipse.getPreferredSize()));

		RectangleFigure rectangle = new RectangleFigure();
		rectangle.setBackgroundColor(ColorConstants.black);
		rectangle.setPreferredSize(7, 6);
		add(rectangle,
				new Rectangle(new Point(this.getLocation().x+12, this
						.getLocation().y+4), rectangle.getPreferredSize()));
		
	}
	
}
