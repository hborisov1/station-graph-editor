package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;

/**
 * Figure for displaying a blue button
 * 
 * @author Hristo Borisov
 *
 */
public class BlueButtonFigure extends ButtonFigure {

	public BlueButtonFigure() {
		button.setBackgroundColor(ColorConstants.blue);
	}
	
}
