package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;

/**
 * Figure for displaying a black button
 * 
 * @author Hristo Borisov
 *
 */
public class BlackButtonFigure extends ButtonFigure {
	
	public BlackButtonFigure() {
		button.setBackgroundColor(ColorConstants.black);
	}

}
