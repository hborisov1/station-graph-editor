package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;

/**
 * Figure for displaying a green button
 * 
 * @author Hristo Borisov
 *
 */
public class GreenButtonFigure extends ButtonFigure {

	public GreenButtonFigure() {
		button.setBackgroundColor(ColorConstants.green);
	}
	
}
