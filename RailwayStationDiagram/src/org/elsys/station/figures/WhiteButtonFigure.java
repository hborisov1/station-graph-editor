package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;

/**
 * Figure for displaying an white button
 * 
 * @author Hristo Borisov
 *
 */
public class WhiteButtonFigure extends ButtonFigure {

	public WhiteButtonFigure() {
		button.setBackgroundColor(ColorConstants.white);
	}
	
}
