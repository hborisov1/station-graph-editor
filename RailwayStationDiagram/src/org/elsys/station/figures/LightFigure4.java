package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

public class LightFigure4 extends Figure {

	public LightFigure4() {
		setLayoutManager(new XYLayout());
		
		RectangleFigure bigRect = new RectangleFigure();
		bigRect.setBackgroundColor(ColorConstants.black);
		bigRect.setForegroundColor(ColorConstants.white);
		bigRect.setPreferredSize(14,13);
//		add(bigRect, new Rectangle(new Point(this.getLocation().x+6, this
//				.getLocation().y+25), bigRect.getPreferredSize()));
		add(bigRect,
				new Rectangle(new Point(this.getLocation().x+24, this
						.getLocation().y+25), bigRect.getPreferredSize()));

		RectangleFigure rectangle = new RectangleFigure();
		rectangle.setBackgroundColor(ColorConstants.black);
		rectangle.setPreferredSize(7, 6);
//		add(rectangle,
//				new Rectangle(new Point(this.getLocation().x+1, this
//						.getLocation().y+29), rectangle.getPreferredSize()));
		add(rectangle,
				new Rectangle(new Point(this.getLocation().x+19, this
						.getLocation().y+28), rectangle.getPreferredSize()));
		setOpaque(true);
	}
	
}
