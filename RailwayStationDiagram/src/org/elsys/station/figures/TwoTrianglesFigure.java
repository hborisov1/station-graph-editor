package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.PolygonShape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

public class TwoTrianglesFigure extends Figure {

	private PolygonShape redTriangle;
	private PolygonShape whiteTriangle;
	
	public TwoTrianglesFigure() {
		setLayoutManager(new StackLayout());
		Rectangle r = new Rectangle(0,0,40,40);

		redTriangle = new PolygonShape();
		redTriangle.setStart(new Point(0,15));
		redTriangle.addPoint(new Point(0,15));
		redTriangle.addPoint(new Point(20,20));
		redTriangle.addPoint(new Point(0,25));
		redTriangle.setEnd(new Point(0,25));
		redTriangle.setFill(true);
		redTriangle.setPreferredSize(r.getSize());
		redTriangle.setBackgroundColor(ColorConstants.red);
		redTriangle.setForegroundColor(ColorConstants.gray);
		add(redTriangle);
		
		whiteTriangle = new PolygonShape();
		whiteTriangle.setStart(new Point(40,15));
		whiteTriangle.addPoint(new Point(40,15));
		whiteTriangle.addPoint(new Point(20,20));
		whiteTriangle.addPoint(new Point(40,25));
		whiteTriangle.setEnd(new Point(40,25));
		whiteTriangle.setFill(true);
		whiteTriangle.setPreferredSize(r.getSize());
		whiteTriangle.setBackgroundColor(ColorConstants.white);
		whiteTriangle.setForegroundColor(ColorConstants.gray);
		add(whiteTriangle);
	}
	
}
