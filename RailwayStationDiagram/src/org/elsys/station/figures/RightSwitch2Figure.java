package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.PolygonShape;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

public class RightSwitch2Figure extends PolygonShape {

	public RightSwitch2Figure() {
		Rectangle r = new Rectangle(0,0,40,40);
		setStart(new Point(15,0));
		addPoint(new Point(15,0));
		addPoint(new Point(0,15));
		addPoint(new Point(0,25));
		addPoint(new Point(25,0));
		addPoint(new Point(15,0));
		setEnd(new Point(15,0));
		setFill(true);
		setPreferredSize(r.getSize());
		setBackgroundColor(ColorConstants.black);
	}
	
}
