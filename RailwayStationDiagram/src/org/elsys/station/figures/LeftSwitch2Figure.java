package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.PolygonShape;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

public class LeftSwitch2Figure extends PolygonShape {

	public LeftSwitch2Figure(){
		Rectangle r = new Rectangle(0,0,40,40);
		setStart(new Point(14,0));
		addPoint(new Point(14,0));
		addPoint(new Point(40,26));
		addPoint(new Point(40,16));
		addPoint(new Point(24,0));
		addPoint(new Point(14,0));
		setEnd(new Point(14,0));
		setFill(true);
		setPreferredSize(r.getSize());
		setBackgroundColor(ColorConstants.black);
	}
	
	
}
