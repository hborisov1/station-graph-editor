package org.elsys.station.figures;

import org.eclipse.draw2d.Label;

/**
 * Figure for displaying a square number
 * 
 * @author Hristo Borisov
 *
 */
public class SquareIndexFigure extends Label {

	public SquareIndexFigure(int index) {
		super(Integer.toString(index));
	}

}
