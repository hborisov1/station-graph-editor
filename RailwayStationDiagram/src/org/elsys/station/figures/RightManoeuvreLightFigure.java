package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * Figure for displaying a right manoeuvre light
 * 
 * @author Hristo Borisov
 *
 */
public class RightManoeuvreLightFigure extends Figure {

	public RightManoeuvreLightFigure() {
		setLayoutManager(new XYLayout());

		RectangleFigure bigRect = new RectangleFigure();
		bigRect.setBackgroundColor(ColorConstants.black);
		bigRect.setForegroundColor(ColorConstants.white);
		bigRect.setPreferredSize(15, 14);
		add(bigRect,
				new Rectangle(this.getLocation(), bigRect.getPreferredSize()));

		RectangleFigure rectangle = new RectangleFigure();
		rectangle.setBackgroundColor(ColorConstants.black);
		rectangle.setPreferredSize(7, 6);
		add(rectangle,
				new Rectangle(new Point(this.getLocation().x+12, this
						.getLocation().y+4), rectangle.getPreferredSize()));
		setOpaque(true);
	}

}
