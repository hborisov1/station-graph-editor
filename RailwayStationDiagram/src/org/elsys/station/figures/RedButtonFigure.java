package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;

/**
 * Figure for displaying a red button
 * 
 * @author Hristo Borisov
 *
 */
public class RedButtonFigure extends ButtonFigure {

	public RedButtonFigure() {
		button.setBackgroundColor(ColorConstants.red);
	}
	
}
