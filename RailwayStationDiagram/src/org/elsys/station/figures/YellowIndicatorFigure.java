package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * Figure for displaying an yellow indicator
 * 
 * @author Hristo Borisov
 *
 */
public class YellowIndicatorFigure extends Figure {

	public YellowIndicatorFigure() {
		setLayoutManager(new XYLayout());

		Ellipse ellipse = new Ellipse();
		ellipse.setBackgroundColor(ColorConstants.yellow);
		ellipse.setForegroundColor(ColorConstants.white);
		ellipse.setPreferredSize(11, 11);
		add(ellipse,
				new Rectangle(this.getLocation(), ellipse.getPreferredSize()));

	}

}
