package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;

/**
 * Draws a square
 * 
 * @author Hristo Borisov
 *
 */
public class SquareFigure extends Figure {

	/** Container for elements */
	private final IFigure elementsContainer;
	
	/** Border of the square */
	private final LineBorder lineBorder;

	/**
	 * Creates SquareFigure
	 * 
	 * @param x
	 * 			x coordinate of the square
	 * @param y
	 * 			y coordinate of the square
	 */
	public SquareFigure(int x, int y) {

		setBackgroundColor(ColorConstants.white);
		final StackLayout layout = new StackLayout();
		setLayoutManager(layout);

		this.setLocation(new Point(x, y));
		this.setSize(new Dimension(40, 40));

		lineBorder = new LineBorder(ColorConstants.black,1);
		setBorder(lineBorder);

		// Add a container for elements
		elementsContainer = new Figure();
		final StackLayout notesLayout = new StackLayout();
		elementsContainer.setLayoutManager(notesLayout);
		add(elementsContainer);

	}

	/**
	 * @return
	 * 		elements container for this square
	 */
	public IFigure getElementsContainer() {
		return elementsContainer;
	}

}
