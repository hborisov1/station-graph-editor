package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.PolygonShape;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

public class RightSwitch1Figure extends PolygonShape {

	public RightSwitch1Figure() {
		Rectangle r = new Rectangle(0,0,40,40);
		setStart(new Point(14,40));
		addPoint(new Point(14,40));
		addPoint(new Point(40,14));
		addPoint(new Point(40,24));
		addPoint(new Point(24,40));
		addPoint(new Point(14,40));
		setEnd(new Point(14,40));
		setFill(true);
		setPreferredSize(r.getSize());
		setBackgroundColor(ColorConstants.black);
	}
	
}
