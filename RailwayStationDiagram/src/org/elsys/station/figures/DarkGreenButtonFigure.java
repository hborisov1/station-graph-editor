package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;

/**
 * Figure for displaying a dark green button
 * 
 * @author Hristo Borisov
 *
 */
public class DarkGreenButtonFigure extends ButtonFigure {
	
	public DarkGreenButtonFigure() {
		button.setBackgroundColor(ColorConstants.darkGreen);
	}

}
