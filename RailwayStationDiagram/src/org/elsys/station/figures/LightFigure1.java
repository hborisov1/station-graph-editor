package org.elsys.station.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

public class LightFigure1 extends Figure {

	public LightFigure1() {
		setLayoutManager(new XYLayout());
		
		Ellipse ellipse = new Ellipse();
		ellipse.setBackgroundColor(ColorConstants.black);
		ellipse.setForegroundColor(ColorConstants.white);
//		ellipse.setBorder(new LineBorder(ColorConstants.lightGray,1));
		ellipse.setPreferredSize(15, 14);
		add(ellipse,
				new Rectangle(this.getLocation(), ellipse.getPreferredSize()));

		RectangleFigure rectangle = new RectangleFigure();
		rectangle.setBackgroundColor(ColorConstants.black);
		rectangle.setPreferredSize(7, 6);
		add(rectangle,
				new Rectangle(new Point(this.getLocation().x+12, this
						.getLocation().y+4), rectangle.getPreferredSize()));
		
	}
	
//	@Override
//	protected void paintFigure(Graphics graphics) {
//		graphics.setAntialias(SWT.ON);
//	}

}
