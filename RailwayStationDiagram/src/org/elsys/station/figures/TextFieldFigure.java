package org.elsys.station.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * Figure for displaying a text field
 * 
 * @author Hristo Borisov
 * 
 */
public class TextFieldFigure extends Figure {

	/** Label for the text */
	private Label label;

	public TextFieldFigure() {
		setLayoutManager(new ToolbarLayout());
		label = new Label();
		add(label);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Figure#paintFigure(org.eclipse.draw2d.Graphics)
	 */
	@Override
	protected void paintFigure(Graphics graphics) {
		Rectangle r = getBounds().getCopy();
		setConstraint(label, new Rectangle(0, 0, r.width, r.height));
	}

	/**
	 * @return the label of the text field
	 */
	public Label getLabel() {
		return label;
	}

}
