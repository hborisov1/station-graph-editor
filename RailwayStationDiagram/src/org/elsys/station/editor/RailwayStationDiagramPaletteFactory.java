package org.elsys.station.editor;

import org.eclipse.gef.palette.CombinedTemplateCreationEntry;
import org.eclipse.gef.palette.MarqueeToolEntry;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteToolbar;
import org.eclipse.gef.palette.PanningSelectionToolEntry;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gef.requests.SimpleFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.elsys.station.model.RightEnd;
import org.elsys.station.model.BlackButton;
import org.elsys.station.model.BlueButton;
import org.elsys.station.model.DarkGreenButton;
import org.elsys.station.model.LeftEnd;
import org.elsys.station.model.GreenButton;
import org.elsys.station.model.GreenIndicator;
import org.elsys.station.model.LowerLeftSwitch;
import org.elsys.station.model.UpperRightSwitch;
import org.elsys.station.model.RightExitLight;
import org.elsys.station.model.LeftExitLight;
import org.elsys.station.model.RightManoeuvreLight;
import org.elsys.station.model.LeftManoeuvreLight;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RedButton;
import org.elsys.station.model.RedIndicator;
import org.elsys.station.model.LowerRightSwitch;
import org.elsys.station.model.UpperLeftSwitch;
import org.elsys.station.model.TextField;
import org.elsys.station.model.Crossing;
import org.elsys.station.model.WhiteButton;
import org.elsys.station.model.YellowButton;
import org.elsys.station.model.YellowIndicator;

/**
 * Class that creates the editor's palette
 * 
 * @author Hristo Borisov
 *
 */
final class RailwayStationDiagramPaletteFactory {

	private RailwayStationDiagramPaletteFactory() {
	}

	/** Create the palette
	 * @return
	 * 		palette
	 */
	static PaletteRoot createPalette() {
		PaletteRoot palette = new PaletteRoot();
		palette.add(createToolsGroup(palette));
		palette.add(createElementsDrawer(palette));
		palette.add(createLightsDrawer());
		palette.add(createIndicatorsDrawer());
		palette.add(createButtonsDrawer(palette));
		palette.add(createOthersDrawer());
		return palette;
	}

	/** Create container with tools used for creating other figures*/
	private static PaletteEntry createOthersDrawer() {
		PaletteDrawer componetsDrawer = new PaletteDrawer("Others",
				ImageDescriptor.createFromFile(Activator.class,
						"icons/others.gif"));

		CombinedTemplateCreationEntry component = new CombinedTemplateCreationEntry(
				"Text", "Create textbox", TextField.class, new SimpleFactory(
						TextField.class), ImageDescriptor.createFromFile(
						Activator.class, "icons/text.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/text.gif"));
		componetsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Crossing",
				"Create crossing figure", Crossing.class, new SimpleFactory(
						Crossing.class), ImageDescriptor.createFromFile(
						Activator.class, "icons/triangles.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/triangles.gif"));
		componetsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Left End", "Create left end figure",
				RightEnd.class, new SimpleFactory(RightEnd.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/begin.gif"), ImageDescriptor.createFromFile(
						Activator.class, "icons/begin.gif"));
		componetsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Right End", "Create right end figure",
				LeftEnd.class, new SimpleFactory(LeftEnd.class),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/end.gif"),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/end.gif"));
		componetsDrawer.add(component);

		return componetsDrawer;

	}

	/** Create container with tools used for creating buttons */
	private static PaletteEntry createButtonsDrawer(PaletteRoot palette) {
		PaletteDrawer componetsDrawer = new PaletteDrawer("Buttons",
				ImageDescriptor.createFromFile(Activator.class,
						"icons/buttons.gif"));

		CombinedTemplateCreationEntry component1 = new CombinedTemplateCreationEntry(
				"Green Button", "Create a green button", GreenButton.class,
				new SimpleFactory(GreenButton.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/greenButton.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/greenButton.gif"));
		componetsDrawer.add(component1);
		// palette.setDefaultEntry(component1);

		CombinedTemplateCreationEntry component = new CombinedTemplateCreationEntry(
				"Red Button", "Create a red button", RedButton.class,
				new SimpleFactory(RedButton.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/redButton.gif"), ImageDescriptor.createFromFile(
						Activator.class, "icons/redButton.gif"));
		componetsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Blue Button",
				"Create a blue button", BlueButton.class, new SimpleFactory(
						BlueButton.class), ImageDescriptor.createFromFile(
						Activator.class, "icons/blueButton.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/blueButton.gif"));
		componetsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Yellow Button",
				"Create a yellow button", YellowButton.class,
				new SimpleFactory(YellowButton.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/yellowButton.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/yellowButton.gif"));
		componetsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("White Button",
				"Create a white button", WhiteButton.class, new SimpleFactory(
						WhiteButton.class), ImageDescriptor.createFromFile(
						Activator.class, "icons/whiteButton.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/whiteButton.gif"));
		componetsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Black Button",
				"Create a black button", BlackButton.class, new SimpleFactory(
						BlackButton.class), ImageDescriptor.createFromFile(
						Activator.class, "icons/blackButton.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/blackButton.gif"));
		componetsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Dark Green Button",
				"Create a dark green button", DarkGreenButton.class,
				new SimpleFactory(DarkGreenButton.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/darkGreenButton.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/darkGreenButton.gif"));
		componetsDrawer.add(component);

		return componetsDrawer;
	}

	/** Create container with tools used for creating indicators */
	private static PaletteEntry createIndicatorsDrawer() {
		PaletteDrawer componentsDrawer = new PaletteDrawer("Indicators",
				ImageDescriptor.createFromFile(Activator.class,
						"icons/indicators.gif"));

		CombinedTemplateCreationEntry component = new CombinedTemplateCreationEntry(
				"Red Indicator", "Create a red indicator", RedIndicator.class,
				new SimpleFactory(RedIndicator.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/redIndicator.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/redIndicator.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Green Indicator",
				"Create a green indicator", GreenIndicator.class,
				new SimpleFactory(GreenIndicator.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/greenIndicator.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/greenIndicator.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Yellow Indicator",
				"Create a yellow indicator", YellowIndicator.class,
				new SimpleFactory(YellowIndicator.class),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/yellowIndicator.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/yellowIndicator.gif"));
		componentsDrawer.add(component);

		return componentsDrawer;
	}

	/** Create container with tools used for creating lights */
	private static PaletteEntry createLightsDrawer() {
		PaletteDrawer componentsDrawer = new PaletteDrawer("Lights",
				ImageDescriptor.createFromFile(Activator.class,
						"icons/lights.gif"));

		CombinedTemplateCreationEntry component = new CombinedTemplateCreationEntry(
				"Right Exit Light",
				"Create a right exit light figure",
				RightExitLight.class,
				new SimpleFactory(RightExitLight.class),
				ImageDescriptor.createFromFile(Activator.class, "icons/l1.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/light1.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry(
				"Left Exit Light",
				"Create a left exit light figure",
				LeftExitLight.class,
				new SimpleFactory(LeftExitLight.class),
				ImageDescriptor.createFromFile(Activator.class, "icons/l2.gif"),
				ImageDescriptor.createFromFile(Activator.class, "icons/l2.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry(
				"Right Manoeuvre Light",
				"Create a right manoeuvre light figure",
				RightManoeuvreLight.class,
				new SimpleFactory(RightManoeuvreLight.class),
				ImageDescriptor.createFromFile(Activator.class, "icons/l3.gif"),
				ImageDescriptor.createFromFile(Activator.class, "icons/l3.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry(
				"Left Manoeuvre Light",
				"Create a left manoeuvre light figure",
				LeftManoeuvreLight.class,
				new SimpleFactory(LeftManoeuvreLight.class),
				ImageDescriptor.createFromFile(Activator.class, "icons/l4.gif"),
				ImageDescriptor.createFromFile(Activator.class, "icons/l4.gif"));
		componentsDrawer.add(component);

		return componentsDrawer;
	}

	/** Create container with tools used for creating rails and switches */
	private static PaletteEntry createElementsDrawer(PaletteRoot palette) {
		PaletteDrawer componentsDrawer = new PaletteDrawer(
				"Rails and Switches", ImageDescriptor.createFromFile(
						Activator.class, "icons/railsAndSwitches.gif"));

		CombinedTemplateCreationEntry component1 = new CombinedTemplateCreationEntry(
				"Rail", "Create a rail figure", Rail.class, new SimpleFactory(
						Rail.class), ImageDescriptor.createFromFile(
						Activator.class, "icons/rail.gif"),
				ImageDescriptor.createFromFile(Activator.class,
						"icons/rail.gif"));
		componentsDrawer.add(component1);

		CombinedTemplateCreationEntry component = new CombinedTemplateCreationEntry(
				"Lower Left Switch", "Create a lower left switch figure",
				LowerLeftSwitch.class,
				new SimpleFactory(LowerLeftSwitch.class),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/ls1.gif"),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/ls1.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Upper Right Switch",
				"Create an upper right switch figure", UpperRightSwitch.class,
				new SimpleFactory(UpperRightSwitch.class),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/ls2.gif"),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/ls2.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Lower Right Switch",
				"Create a lower right switch figure", LowerRightSwitch.class,
				new SimpleFactory(LowerRightSwitch.class),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/rs1.gif"),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/rs1.gif"));
		componentsDrawer.add(component);

		component = new CombinedTemplateCreationEntry("Upper Left Switch",
				"Create a upper left switch figure", UpperLeftSwitch.class,
				new SimpleFactory(UpperLeftSwitch.class),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/rs2.gif"),
				ImageDescriptor
						.createFromFile(Activator.class, "icons/rs2.gif"));
		componentsDrawer.add(component);

		return componentsDrawer;
	}

	/** Create toolbar with selection ant marquee tool */
	private static PaletteContainer createToolsGroup(PaletteRoot palette) {
		PaletteToolbar toolbar = new PaletteToolbar("Tools");

		ToolEntry tool = new PanningSelectionToolEntry();
		toolbar.add(tool);

		toolbar.add(new MarqueeToolEntry());

		return toolbar;
	}

}
