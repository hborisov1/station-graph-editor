package org.elsys.station.editor;

import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.ui.palette.PaletteViewer;
import org.eclipse.ui.IEditorPart;

/**
 * The EditDomain used in the editor. This class allows creating multiple 
 * figures with just one click on their tool.
 * 
 * @author Hristo Borisov
 *
 */
public class MyEditDomain extends DefaultEditDomain {

	/** The paletteViewer */
	private PaletteViewer paletteViewer;
	
	/** The paletteRoot */
	private PaletteRoot paletteRoot;

	public MyEditDomain(IEditorPart editorPart) {
		super(editorPart);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.EditDomain#loadDefaultTool()
	 */
	@Override
	public void loadDefaultTool() {
		Tool t = getActiveTool();
		setActiveTool(null);
		if (paletteRoot != null) {
			if (paletteRoot.getDefaultEntry() != null) {
				paletteViewer.setActiveTool(paletteRoot.getDefaultEntry());
				return;
			} else
				paletteViewer.setActiveTool(null);
		}
		setActiveTool(t);
	}

}
