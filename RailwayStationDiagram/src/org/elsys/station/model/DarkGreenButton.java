package org.elsys.station.model;

/**
 * Model element that represents dark green button
 * 
 * @author Hristo Borisov
 *
 */
public class DarkGreenButton extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

}
