package org.elsys.station.model;


/**
 * Model element that represents upper right switch
 * 
 * @author Hristo Borisov
 *
 */
public class UpperRightSwitch extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

}
