package org.elsys.station.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Container for all other model elements
 * 
 * @author Hristo Borisov
 *
 */
public class RailwayStationDiagram extends ModelElement {
	
	private static final long serialVersionUID = 1L;
	
	/** Property ID used when a child is added to the diagram. */
	public static final String CHILD_ADDED_PROP = "StationGraph.ChildAdded";
	
	/** Property ID used when a child is removed from the diagram. */
	public static final String CHILD_REMOVED_PROP = "StationGraph.ChildRemoved";
	
	/** List of elements	 */
	@SuppressWarnings("rawtypes")
	private List elements = new ArrayList();
	
	/** Used to draw the coordinate system of the diagram */
	private Square square;
	
	/** Index for squares in the first column and row */
	private int squareIndex = 0;

	public RailwayStationDiagram() {
		createCoordinateSystem();
	}

	/** Draw the squares, that create the coordinate system of the diagram */
	@SuppressWarnings("unchecked")
	protected void createCoordinateSystem() {
		for (int x = 0; x <= 1280; x = x + 39) {
			for (int y = 0; y <= 939; y = y + 39) {
				square = new Square(x, y);
				addIndexedSquares(square);
				squareIndex++;
				getElements().add(square);
			}
		}

	}

	/**
	 * Add numbers to squares from the first row and column
	 * 
	 * @param square
	 * 				the square which needs a number
	 */
	private void addIndexedSquares(Square square) {
		if (squareIndex < 25)
			square.addIndex(new SquareIndex(squareIndex));
		if(squareIndex%25 == 0)
			square.addIndex(new SquareIndex(squareIndex/25));
	}

	/**
	 * @return 
	 * 			a list of the elements in the diagram
	 */
	@SuppressWarnings("rawtypes")
	public List getElements() {
		return this.elements;
	}

	/**
	 * Add an element to the diagram and notify attached listeners for this change
	 * 
	 * @param elem 
	 * 			the element
	 * @return
	 * 			true if the element was added, false otherwise
	 */
	@SuppressWarnings("unchecked")
	public boolean addChild(RailwayStationElement elem) {
		if (elem != null && elements.add(elem)) {
			firePropertyChange(CHILD_ADDED_PROP, null, elem);
			return true;
		}
		return false;
	}

	/**
	 * Remove an element from the diagram and notify attached listeners for this change
	 * 
	 * @param elem
	 * 			the element
	 * @return
	 * 			true if the element was removed, false otherwise
	 */
	public boolean removeChild(RailwayStationElement elem) {
		if (elem != null && elements.remove(elem)) {
			firePropertyChange(CHILD_REMOVED_PROP, null, elem);
			return true;
		}
		return false;
	}

}
