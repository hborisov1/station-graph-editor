package org.elsys.station.model;

/**
 * Model element that represents left exit light
 * 
 * @author Hristo Borisov
 *
 */
public class LeftExitLight extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

}
