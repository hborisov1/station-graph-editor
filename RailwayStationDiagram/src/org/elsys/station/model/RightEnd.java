package org.elsys.station.model;

/**
 * Model element that represents right end
 * 
 * @author Hristo Borisov
 *
 */
public class RightEnd extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

}
