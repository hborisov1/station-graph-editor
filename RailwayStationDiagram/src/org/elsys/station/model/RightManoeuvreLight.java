package org.elsys.station.model;

/**
 * Model element that represents right manoeuvre light
 * 
 * @author Hristo Borisov
 *
 */
public class RightManoeuvreLight extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

}
