package org.elsys.station.model;

/**
 * Model element that represents square number
 * 
 * @author Hristo Borisov
 *
 */
public class SquareIndex extends ModelElement {

	private static final long serialVersionUID = 1L;
	
	/** Number of the square */
	private int index;

	public SquareIndex(int index) {
		this.index = index;
	}

	/**
	 * @return
	 * 		number of the square
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * Set square number
	 * 
	 * @param index
	 * 		number
	 */
	public void setIndex(int index) {
		this.index = index;
	}
	

}
