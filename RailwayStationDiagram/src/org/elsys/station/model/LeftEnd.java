package org.elsys.station.model;

/**
 * Model element that represents left end
 * 
 * @author Hristo Borisov
 *
 */
public class LeftEnd extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

}
