package org.elsys.station.model;

import org.eclipse.swt.graphics.Image;

public class Light2 extends RailwayStationElement {

	private static final long serialVersionUID = 1L;
	
	private static final Image LIGHT2_ICON = createImage("icons/rail.png");
	
	@Override
	public Image getIcon() {
		return LIGHT2_ICON;
	}
	
	@Override
	public String toString() {
		return "Light2 " + hashCode();
	}

}
