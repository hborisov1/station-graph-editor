package org.elsys.station.model;

import org.eclipse.swt.graphics.Image;

public class Light4 extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

	private static final Image LIGHT4_ICON = createImage("icons/rail.png");

	@Override
	public Image getIcon() {
		return LIGHT4_ICON;
	}

	@Override
	public String toString() {
		return "Light4 " + hashCode();
	}
	
}
