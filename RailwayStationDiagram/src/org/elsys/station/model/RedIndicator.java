package org.elsys.station.model;

/**
 * Model element that represents red indicator
 * 
 * @author Hristo Borisov
 *
 */
public class RedIndicator extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

}
