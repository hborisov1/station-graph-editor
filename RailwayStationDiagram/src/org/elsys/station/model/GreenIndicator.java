package org.elsys.station.model;

/**
 * Model element that represents green indicator
 * 
 * @author Hristo Borisov
 *
 */
public class GreenIndicator extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

}
