package org.elsys.station.model;

/**
 * Model element that represents yellow indicator
 * 
 * @author Hristo Borisov
 *
 */
public class YellowIndicator extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

}
