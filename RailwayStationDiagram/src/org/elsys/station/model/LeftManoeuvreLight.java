package org.elsys.station.model;

/**
 * Model element that represents left manoeuvre light
 * 
 * @author Hristo Borisov
 *
 */
public class LeftManoeuvreLight extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

}
