package org.elsys.station.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Square that can contain other elements
 * 
 * @author Hristo Borisov
 *
 */
public class Square extends ModelElement{
	
	private static final long serialVersionUID = 1L;
	
	/** Property ID used when an element is added in the square. */
	public static final String SNAP_ADDED_PROP = "Square.SnapAdded";

	/** Property ID used when an element is removed from the square. */
	public static final String SNAP_REMOVED_PROP = "Square.SnapRemoved";
	
	/** The x coordinate of the square */
	private int x;
	
	/** The y coordinate of the square */
	private int y;
	
	/** List of elements */
	@SuppressWarnings("rawtypes")
	private List snapElements = new ArrayList();	
	
	public Square(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	/**
	 * @return the x coordinate of the square
	 */
	public int getX(){
		return x;
	}
	
	/**
	 * @return the y coordinate of the square
	 */
	public int getY(){
		return y;
	}
	
	/**
	 * @return a list of elements in the square
	 */
	@SuppressWarnings("rawtypes")
	public List getSnaps(){
		return this.snapElements;
	}
	
	/**
	 * Add an element to the square and notify attached listeners for this change
	 * 
	 * @param snapEelem 
	 * 			the element
	 * @return
	 * 			true if the element was added, false otherwise
	 */
	@SuppressWarnings("unchecked")
	public boolean addSnapElement(RailwayStationElement snapElement){
		if(snapElement!=null && snapElements.add(snapElement)){
			firePropertyChange(SNAP_ADDED_PROP, null, snapElement);
			return true;
		}
		return false;
	}	
	
	/**
	 * Add an element from the square and notify attached listeners for this change
	 * 
	 * @param snapEelem 
	 * 			the element
	 * @return
	 * 			true if the element was removed, false otherwise
	 */
	public boolean removeSnapElement(RailwayStationElement snapElement){
		if(snapElement!=null && snapElements.remove(snapElement)){
			firePropertyChange(SNAP_REMOVED_PROP, null, snapElement);
			return true;
		}
		return false;
	}

	
	/**
	 * Add number to this square
	 * 
	 * @param index
	 * 			number of square
	 * @return
	 * 			true if the number was added, false otherwise
	 */
	@SuppressWarnings("unchecked")
	public boolean addIndex(SquareIndex index){
		if(index!=null && snapElements.add(index)){
			firePropertyChange(SNAP_ADDED_PROP, null, index);
			return true;
		}
		return false;
	}
	
}
