package org.elsys.station.model;

/**
 * Model element that represents upper left switch
 * 
 * @author Hristo Borisov
 * 
 */
public class UpperLeftSwitch extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

}
