package org.elsys.station.model;

/**
 * Model element that represents right exit light
 * 
 * @author Hristo Borisov
 *
 */
public class RightExitLight extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

}
