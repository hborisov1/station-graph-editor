package org.elsys.station.model;


/** 
 * Model element that represents white button
 * 
 * @author Hristo Borisov
 *
 */
public class WhiteButton extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

}
