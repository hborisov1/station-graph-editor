package org.elsys.station.model;

/**
 * Model element that represents lower right switch
 * 
 * @author Hristo Borisov
 *
 */
public class LowerRightSwitch extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

}
