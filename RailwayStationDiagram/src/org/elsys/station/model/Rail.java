package org.elsys.station.model;

/**
 * Model element that represents rail
 * 
 * @author Hristo Borisov
 *
 */
public class Rail extends RailwayStationElement {

	private static final long serialVersionUID = 1;

}