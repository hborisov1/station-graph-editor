package org.elsys.station.model;

/**
 * Model element that represents text field
 * 
 * @author Hristo Borisov
 *
 */
public class TextField extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

	/** Property ID used when the text in the text field is changed. */
	public static final String TEXT_CHANGED_PROP = "RailText.TextChanged";

	/** The text of the text field	 */
	private String text = "���. 1";

	
	/**
	 * @return
	 * 		the text of the text field
	 */
	public String getText() {
		return text;
	}

	/**
	 * Set text to the text field and notify attached listeners for this
	 * 
	 * @param text
	 * 			text
	 */
	public void setText(String text) {
		this.text = text;
		firePropertyChange(TEXT_CHANGED_PROP, null, text);
	}

}
