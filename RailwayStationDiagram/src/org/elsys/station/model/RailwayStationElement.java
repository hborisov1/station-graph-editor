package org.elsys.station.model;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;

/**
 * Abstract prototype of element
 * 
 * @author Hristo Borisov
 * 
 */
public abstract class RailwayStationElement extends ModelElement {

	private static final long serialVersionUID = 1L;

	/** Size of the element */
	private Dimension size = new Dimension(10, 10);

	/** Location of the element	 */
	private Point location = new Point(0, 0);
	

	/**
	 * @return
	 * 		the location of the element
	 */
	public Point getLocation() {
		return location.getCopy();
	}

	/**
	 * Set the location of the element
	 * 
	 * @param newLocation
	 * 			the new location
	 */
	public void setLocation(Point newLocation) {
		if (newLocation == null) {
			throw new IllegalArgumentException();
		}
		location.setLocation(newLocation);
	}

	/**
	 * @return
	 * 		the size of the element
	 */
	public Dimension getSize() {
		return size.getCopy();
	}

	/**
	 * Set the size of the element
	 * 
	 * @param newSize
	 * 			the new size
	 */
	public void setSize(Dimension newSize) {
		if (newSize != null) {
			size.setSize(newSize);
		}
	}

}
