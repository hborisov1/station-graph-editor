package org.elsys.station.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

/**
 * Abstract prototype of a model element.
 * <p>
 * This class provides features necessary for all model elements, like:
 * </p>
 * <ul>
 * <li>property-change support (used to notify edit parts of model changes)</li>
 * <li>serialization support (the model hierarchy must be serializable, so that
 * the editor can save and open binary files.</li>
 * </ul>
 * 
 * @author Hristo Borisov
 * 
 */
public abstract class ModelElement implements Serializable {

	private static final long serialVersionUID = 1L;

	/** Field used for property-change support */
	private transient PropertyChangeSupport pcsDelegate = new PropertyChangeSupport(
			this);

	/**
	 * Attach a PropertyChangeListener to this model element
	 * 
	 * @param l
	 *            a non-null PropertyChangeListener
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		if (l == null) {
			throw new IllegalArgumentException();
		}
		pcsDelegate.addPropertyChangeListener(l);
	}

	/**
	 * Remove a PropertyChangeListener from this object
	 * 
	 * @param l
	 *            a non-null PropertyChangeListener
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		if (l != null) {
			pcsDelegate.removePropertyChangeListener(l);
		}
	}

	/**
	 * Notifies attached listeners(edit parts in this case) if there is a change
	 * in the model
	 * 
	 * @param property
	 *            string ID that describes the changed property in the model
	 * @param oldValue
	 *            the old value of this property
	 * @param newValue
	 *            the new value of this property
	 */
	protected void firePropertyChange(String property, Object oldValue,
			Object newValue) {
		if (pcsDelegate.hasListeners(property)) {
			pcsDelegate.firePropertyChange(property, oldValue, newValue);
		}
	}

	/**
	 * Deserialization constructor. Initializes transient fields.
	 * 
	 * @param in
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void readObject(ObjectInputStream in) throws IOException,
			ClassNotFoundException {
		in.defaultReadObject();
		pcsDelegate = new PropertyChangeSupport(this);
	}

}
