package org.elsys.station.model;

/**
 * Model element that represents lower left switch
 * 
 * @author Hristo Borisov
 *
 */
public class LowerLeftSwitch extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

}
