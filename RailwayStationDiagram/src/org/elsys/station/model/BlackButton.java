package org.elsys.station.model;

/**
 * Model element that represents black button
 * 
 * @author Hristo Borisov
 *
 */
public class BlackButton extends RailwayStationElement {

	private static final long serialVersionUID = 1L;

}
