package org.elsys.station.editpolicy;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ComponentEditPolicy;
import org.eclipse.gef.requests.GroupRequest;
import org.elsys.station.commands.ElementDeleteCommand;
import org.elsys.station.model.RailwayStationElement;
import org.elsys.station.model.Square;

/**
 * EditPolicy used when deleting elements from squares
 * 
 * @author Hristo Borisov
 *
 */
public class RailwayStationComponentEditPolicy extends ComponentEditPolicy {

	/**
	 * @return 
	 * 			command for deleting elements
	 */
	protected Command createDeleteCommand(GroupRequest deleteRequest) {
		Object parent = getHost().getParent().getModel();
		Object child = getHost().getModel();
		if (parent instanceof Square && child instanceof RailwayStationElement) {
			return new ElementDeleteCommand((Square) parent, (RailwayStationElement) child);
		}
		return super.createDeleteCommand(deleteRequest);
	}
}