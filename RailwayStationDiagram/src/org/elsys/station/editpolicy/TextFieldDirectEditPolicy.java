package org.elsys.station.editpolicy;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;
import org.elsys.station.commands.TextChangeCommand;
import org.elsys.station.figures.TextFieldFigure;
import org.elsys.station.model.TextField;

public class TextFieldDirectEditPolicy extends DirectEditPolicy {

	@Override
	protected Command getDirectEditCommand(DirectEditRequest request) {
		TextChangeCommand command = new TextChangeCommand();
	    command.setModel((TextField) getHost().getModel());
	    command.setNewName((String) request.getCellEditor().getValue());
	    return command;
	}

	@Override
	protected void showCurrentEditValue(DirectEditRequest request) {
		String value = (String) request.getCellEditor().getValue();
	    ((TextFieldFigure)getHostFigure()).getLabel().setText(value);
	}

}
