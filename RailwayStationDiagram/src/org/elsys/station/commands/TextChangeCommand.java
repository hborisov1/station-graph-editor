package org.elsys.station.commands;

import org.eclipse.gef.commands.Command;
import org.elsys.station.model.TextField;

/**
 * Command that changes the text of text field figures
 * 
 * @author Hristo Borisov
 *
 */
public class TextChangeCommand extends Command {

	public TextChangeCommand() {
		super("Change text");
	}
	
	/** Old and new text	 */
	private String oldName, newName;
	
	/** Model */
	private TextField model;

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldName = model.getText();
		model.setText(newName);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		model.setText(oldName);
	}

	/**
	 * Set the new text for the text field
	 * 
	 * @param newName
	 * 			the new text
	 */
	public void setNewName(String newName) {
		this.newName = newName;
	}

	/**
	 * Set the model
	 * 
	 * @param model
	 */
	public void setModel(TextField model) {
		this.model = model;
	}

}
