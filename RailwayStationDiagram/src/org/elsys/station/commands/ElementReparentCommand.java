package org.elsys.station.commands;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.elsys.station.model.RailwayStationElement;
import org.elsys.station.model.Square;

/**
 * Command used when an element is moved from one square to another
 * 
 * @author Hristo Borisov
 *
 */
public class ElementReparentCommand extends Command {
	
	/** The new container of the element */
	private final Square container;
	
	/** The old container of the element */
	private Square oldContainer;
	
	/** The moved element */
	private final RailwayStationElement snapElement;
	
	private Rectangle box;

	private Rectangle oldBox;

	public ElementReparentCommand(Square container, RailwayStationElement snapElement) {
		super("Move element");
		this.container = container;
		this.snapElement = snapElement;
	}

	/**
	 * Set the old container
	 * 
	 * @param container
	 * 		old container
	 */
	public void setOldContainer(Square container) {
		oldContainer = container;
	}
	
	public void setBox(Rectangle box) {
		this.box = box;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldBox = new Rectangle(snapElement.getLocation(), snapElement.getSize());
		oldContainer.removeSnapElement(snapElement);
		if(box!=null){
			snapElement.setLocation(box.getLocation());
			snapElement.setSize(box.getSize());
		}
		container.addSnapElement(snapElement);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		container.removeSnapElement(snapElement);
		oldContainer.addSnapElement(snapElement);
		snapElement.setSize(oldBox.getSize());
		snapElement.setLocation(oldBox.getLocation());
	}

}

