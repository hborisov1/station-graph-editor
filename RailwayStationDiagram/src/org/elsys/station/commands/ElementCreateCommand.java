package org.elsys.station.commands;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.elsys.station.model.RailwayStationElement;
import org.elsys.station.model.Square;

/**
 * Command that creates new elements
 * 
 * @author Hristo Borisov
 * 
 */
public class ElementCreateCommand extends Command {

	/** The square where the element must be added	 */
	private final Square square;
	
	/** The new element	 */
	private final RailwayStationElement snapElement;
	
	private final Rectangle box;

	public ElementCreateCommand(Square s, RailwayStationElement snapElement,
			Rectangle box) {
		super("Create new element");
		this.square = s;
		this.snapElement = snapElement;
		this.box = box;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		if (box != null) {
			snapElement.setLocation(box.getLocation());
			snapElement.setSize(box.getSize());
		}
		square.addSnapElement(snapElement);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		square.removeSnapElement(snapElement);
	}

}
