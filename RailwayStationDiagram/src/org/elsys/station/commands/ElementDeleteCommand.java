package org.elsys.station.commands;

import org.eclipse.gef.commands.Command;
import org.elsys.station.model.RailwayStationElement;
import org.elsys.station.model.Square;

/**
 * Command used to delete elements
 * 
 * @author Hristo Borisov
 *
 */
public class ElementDeleteCommand extends Command {

	
	/** Element that must be deleted */
	private final RailwayStationElement element;

	/** The square container of the element	 */
	private final Square square;

	/** Used to indicate if the element was removed*/
	private boolean wasRemoved;

	public ElementDeleteCommand(Square square, RailwayStationElement element) {
		super("Delete element");
		if (square == null || element == null) {
			throw new IllegalArgumentException();
		}
		this.square = square;
		this.element = element;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	public boolean canUndo() {
		return wasRemoved;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		redo();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	public void redo() {
		wasRemoved = square.removeSnapElement(element);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	public void undo() {
		square.addSnapElement(element);
	}
}