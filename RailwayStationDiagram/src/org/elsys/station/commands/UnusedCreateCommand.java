package org.elsys.station.commands;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;
import org.elsys.station.model.RailwayStationElement;
import org.elsys.station.model.RailwayStationDiagram;

/**
 * This class is not used by default. If you want to use id you must enable
 * XYLayoutEditPolicy in StationGraphEditPart. 
 * 
 * @author Hristo Borisov
 * 
 */
public class UnusedCreateCommand extends Command {

	private RailwayStationElement newElement;
	private final RailwayStationDiagram graph;
	private Rectangle bounds;

	public UnusedCreateCommand(RailwayStationElement newElement, RailwayStationDiagram graph,
			Rectangle bounds) {
		this.newElement = newElement;
		this.graph = graph;
		this.bounds = bounds;
		setLabel("element creation");
	}

	public boolean canExecute() {
		return newElement != null && graph != null && bounds != null;
	}

	public void execute() {
		newElement.setLocation(bounds.getLocation());
		Dimension size = bounds.getSize();
		if (size.width > 0 && size.height > 0)
			newElement.setSize(size);
		redo();
	}

	public void redo() {
		graph.addChild(newElement);
	}

	public void undo() {
		graph.removeChild(newElement);
	}

}