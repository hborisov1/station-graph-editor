package org.elsys.station.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.elsys.station.editpolicy.RailwayStationComponentEditPolicy;
import org.elsys.station.figures.BlackButtonFigure;
import org.elsys.station.figures.BlueButtonFigure;
import org.elsys.station.figures.DarkGreenButtonFigure;
import org.elsys.station.figures.GreenButtonFigure;
import org.elsys.station.figures.GreenIndicatorFigure;
import org.elsys.station.figures.LeftEndFigure;
import org.elsys.station.figures.LeftExitLightFigure;
import org.elsys.station.figures.LeftManoeuvreLightFigure;
import org.elsys.station.figures.LowerLeftSwitchFigure;
import org.elsys.station.figures.LowerRightSwitchFigure;
import org.elsys.station.figures.RailFigure;
import org.elsys.station.figures.RedButtonFigure;
import org.elsys.station.figures.RedIndicatorFigure;
import org.elsys.station.figures.RightEndFigure;
import org.elsys.station.figures.RightExitLightFigure;
import org.elsys.station.figures.RightManoeuvreLightFigure;
import org.elsys.station.figures.CrossingFigure;
import org.elsys.station.figures.UpperLeftSwitchFigure;
import org.elsys.station.figures.UpperRightSwitchFigure;
import org.elsys.station.figures.WhiteButtonFigure;
import org.elsys.station.figures.YellowButtonFigure;
import org.elsys.station.figures.YellowIndicatorFigure;
import org.elsys.station.model.BlackButton;
import org.elsys.station.model.BlueButton;
import org.elsys.station.model.Crossing;
import org.elsys.station.model.DarkGreenButton;
import org.elsys.station.model.GreenButton;
import org.elsys.station.model.GreenIndicator;
import org.elsys.station.model.LeftEnd;
import org.elsys.station.model.LeftExitLight;
import org.elsys.station.model.LeftManoeuvreLight;
import org.elsys.station.model.LowerLeftSwitch;
import org.elsys.station.model.LowerRightSwitch;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RedButton;
import org.elsys.station.model.RedIndicator;
import org.elsys.station.model.RightEnd;
import org.elsys.station.model.RightExitLight;
import org.elsys.station.model.RightManoeuvreLight;
import org.elsys.station.model.UpperLeftSwitch;
import org.elsys.station.model.UpperRightSwitch;
import org.elsys.station.model.WhiteButton;
import org.elsys.station.model.YellowButton;
import org.elsys.station.model.YellowIndicator;

/**
 * EditPart for the RailwayStationElement instance.
 * 
 * @author Hristo Borisov
 *
 */
public class RailwayStationElementsEditPart extends AbstractGraphicalEditPart {

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		IFigure f = createFigureForModel();
		f.setOpaque(false);
		return f;
	}

	/**
	 * Create figure for appropriate model element
	 * 
	 * @return
	 * 		figure
	 */
	private IFigure createFigureForModel() {
		if (getModel() instanceof Rail) {
			return new RailFigure();
		} else if (getModel() instanceof LowerLeftSwitch) {
			return new LowerLeftSwitchFigure();
		} else if (getModel() instanceof UpperRightSwitch) {
			return new UpperRightSwitchFigure();
		} else if (getModel() instanceof LowerRightSwitch) {
			return new LowerRightSwitchFigure();
		} else if (getModel() instanceof UpperLeftSwitch) {
			return new UpperLeftSwitchFigure();
		} else if (getModel() instanceof RightExitLight) {
			return new RightExitLightFigure();
		} else if (getModel() instanceof LeftExitLight) {
			return new LeftExitLightFigure();
		} else if (getModel() instanceof RightManoeuvreLight) {
			return new RightManoeuvreLightFigure();
		} else if (getModel() instanceof LeftManoeuvreLight) {
			return new LeftManoeuvreLightFigure();
		} else if (getModel() instanceof RedIndicator) {
			return new RedIndicatorFigure();
		} else if (getModel() instanceof GreenIndicator) {
			return new GreenIndicatorFigure();
		} else if (getModel() instanceof YellowIndicator) {
			return new YellowIndicatorFigure();
		} else if (getModel() instanceof GreenButton) {
			return new GreenButtonFigure();
		} else if (getModel() instanceof WhiteButton) {
			return new WhiteButtonFigure();
		} else if (getModel() instanceof BlueButton) {
			return new BlueButtonFigure();
		} else if (getModel() instanceof RedButton) {
			return new RedButtonFigure();
		} else if (getModel() instanceof YellowButton) {
			return new YellowButtonFigure();
		} else if (getModel() instanceof Crossing) {
			return new CrossingFigure();
		} else if (getModel() instanceof RightEnd) {
			return new LeftEndFigure();
		} else if (getModel() instanceof LeftEnd) {
			return new RightEndFigure();
		} else if (getModel() instanceof BlackButton) {
			return new BlackButtonFigure();
		} else if (getModel() instanceof DarkGreenButton) {
			return new DarkGreenButtonFigure();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * EditPolicy that handles element deletion
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new RailwayStationComponentEditPolicy());
	}

}
