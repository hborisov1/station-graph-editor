package org.elsys.station.editparts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.jface.viewers.TextCellEditor;
import org.elsys.station.editpolicy.TextFieldCellEditorLocator;
import org.elsys.station.editpolicy.TextFieldDirectEditManager;
import org.elsys.station.editpolicy.TextFieldDirectEditPolicy;
import org.elsys.station.figures.TextFieldFigure;
import org.elsys.station.model.ModelElement;
import org.elsys.station.model.TextField;

public class TextFieldEditPart extends AbstractGraphicalEditPart implements
		PropertyChangeListener {

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String prop = evt.getPropertyName();
		if(TextField.TEXT_CHANGED_PROP.equals(prop)){
			refreshVisuals();
		}
	}

	protected void refreshVisuals() {
		TextFieldFigure figure = (TextFieldFigure) getFigure();
		TextField model = (TextField) getModel();
		SquareEditPart parent = (SquareEditPart) getParent();

		figure.getLabel().setText(model.getText());
		Rectangle layout = new Rectangle(10, 10, 20, 20);
		parent.setLayoutConstraint(this, figure, layout);
	}

	@Override
	protected IFigure createFigure() {
		return new TextFieldFigure();
	}

	public void activate() {
		if (!isActive()) {
			super.activate();
			((ModelElement) getModel()).addPropertyChangeListener(this);
		}
	}

	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((ModelElement) getModel()).removePropertyChangeListener(this);
		}
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE,
				new TextFieldDirectEditPolicy());
	}

	@Override
	public void performRequest(Request req) {
		if (req.getType() == RequestConstants.REQ_OPEN) {
			performDirectEditing();
		}
	}

	private void performDirectEditing() {
		Label label = ((TextFieldFigure) getFigure()).getLabel();
		TextFieldDirectEditManager manager = new TextFieldDirectEditManager(this,
				TextCellEditor.class, new TextFieldCellEditorLocator(label),
				label);
		manager.show();
	}

}
