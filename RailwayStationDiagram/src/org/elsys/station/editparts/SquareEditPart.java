package org.elsys.station.editparts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gef.editpolicies.OrderedLayoutEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.elsys.station.commands.ElementCreateCommand;
import org.elsys.station.commands.ElementReparentCommand;
import org.elsys.station.figures.SquareFigure;
import org.elsys.station.model.BlackButton;
import org.elsys.station.model.BlueButton;
import org.elsys.station.model.Crossing;
import org.elsys.station.model.DarkGreenButton;
import org.elsys.station.model.GreenButton;
import org.elsys.station.model.GreenIndicator;
import org.elsys.station.model.LeftEnd;
import org.elsys.station.model.LeftExitLight;
import org.elsys.station.model.LeftManoeuvreLight;
import org.elsys.station.model.LowerLeftSwitch;
import org.elsys.station.model.LowerRightSwitch;
import org.elsys.station.model.ModelElement;
import org.elsys.station.model.Rail;
import org.elsys.station.model.RailwayStationElement;
import org.elsys.station.model.RedButton;
import org.elsys.station.model.RedIndicator;
import org.elsys.station.model.RightEnd;
import org.elsys.station.model.RightExitLight;
import org.elsys.station.model.RightManoeuvreLight;
import org.elsys.station.model.Square;
import org.elsys.station.model.TextField;
import org.elsys.station.model.UpperLeftSwitch;
import org.elsys.station.model.UpperRightSwitch;
import org.elsys.station.model.WhiteButton;
import org.elsys.station.model.YellowButton;
import org.elsys.station.model.YellowIndicator;

/**
 * EditPart used for squares
 * 
 * @author Hristo Borisov
 *
 */
public class SquareEditPart extends AbstractGraphicalEditPart implements
		PropertyChangeListener {

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		int x = ((Square) getModel()).getX();
		int y = ((Square) getModel()).getY();
		return new SquareFigure(x, y);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModel()
	 */
	public Square getModel() {
		return (Square) super.getModel();
	}

	/**
	 * @return
	 * 		the SquareFigure for this square
	 */
	private SquareFigure getSquareFigure() {
		return (SquareFigure) getFigure();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#getContentPane()
	 */
	@Override
	public IFigure getContentPane() {
		return getSquareFigure().getElementsContainer();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected List getModelChildren() {
		List elements = ((Square) getModel()).getSnaps();
		return elements;
	}

	
	/**
	 * EditPolicy that handles creating and reparenting of elements
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.LAYOUT_ROLE,
				new OrderedLayoutEditPolicy() {

					@Override
					protected Command getCreateCommand(CreateRequest request) {
						Object type = request.getNewObjectType();
						if (type == Rail.class || type == LowerLeftSwitch.class
								|| type == UpperRightSwitch.class
								|| type == LowerRightSwitch.class
								|| type == UpperLeftSwitch.class
								|| type == RightExitLight.class || type == LeftExitLight.class
								|| type == RightManoeuvreLight.class || type == LeftManoeuvreLight.class
								|| type == RedIndicator.class
								|| type == GreenIndicator.class
								|| type == YellowIndicator.class
								|| type == TextField.class
								|| type == GreenButton.class
								|| type == WhiteButton.class
								|| type == BlueButton.class
								|| type == RedButton.class
								|| type == YellowButton.class
								|| type == Crossing.class
								|| type == RightEnd.class 
								|| type == LeftEnd.class
								|| type == BlackButton.class
								|| type == DarkGreenButton.class) {
							RailwayStationElement snapElement = (RailwayStationElement) request
									.getNewObject();
							return new ElementCreateCommand(
									(Square) getModel(), snapElement, null);
						}
						return null;
					}

					@Override
					protected EditPart getInsertionReference(Request request) {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					protected Command createMoveChildCommand(EditPart child,
							EditPart after) {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					protected Command createAddCommand(EditPart child,
							EditPart after) {
						Square oldContainer = (Square) child.getParent()
								.getModel();
						if (getModel() == oldContainer)
							return null;
						RailwayStationElement snapElement = (RailwayStationElement) child
								.getModel();
						ElementReparentCommand rrc = new ElementReparentCommand(
								(Square) getModel(), snapElement);
						rrc.setOldContainer(oldContainer);
						return rrc;
					}
				});
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#isSelectable()
	 */
	@Override
	public boolean isSelectable() {
		return false;
	}

	/* (non-Javadoc)
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
		String prop = arg0.getPropertyName();
		if (Square.SNAP_ADDED_PROP.equals(prop)
				|| Square.SNAP_REMOVED_PROP.equals(prop))
			refreshChildren();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		if (!isActive()) {
			super.activate();
			((ModelElement) getModel()).addPropertyChangeListener(this);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#deactivate()
	 */
	public void deactivate() {
		if (isActive()) {
			super.deactivate();
			((ModelElement) getModel()).removePropertyChangeListener(this);
		}
	}

}
