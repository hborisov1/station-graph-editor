package org.elsys.station.editparts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.elsys.station.figures.SquareIndexFigure;
import org.elsys.station.model.SquareIndex;

/**
 * EditPart for the SquareIndex instance.
 * 
 * @author Hristo Borisov
 *
 */
public class SquareIndexEditPart extends AbstractGraphicalEditPart {

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		int index = ((SquareIndex) getModel()).getIndex();
		return new SquareIndexFigure(index);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModel()
	 */
	public SquareIndex getModel() {
		return (SquareIndex) super.getModel();
	}

	/** 
	 * Square numbers aren't selectable
	 */
	@Override
	public boolean isSelectable() {
		return false;
	}
	
}
